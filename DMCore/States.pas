unit States;

interface

type
  TDownloadError = (deGetCode, // �� ������� �������� ��� ������ �������
    deCodeFailed, // �� ���������� ��� �������
    deInetReadFile, // ������ ������ ����� � �������
    deCreateDownloadPath, // ������ �������� ����� ��� �������� �����
    deCreateFileInfo, deSaveFileInfo, deReadFileInfo);

  // ��������� �����
  TFileState = (fsGetInfo, fsDownloading, fsComplete, fsStopped, fsError);

  // ��������� �����
  TPartState = (psConnecting, psConnected, psDownloading, psCompleted,
    psStopped, psError);

implementation

end.
