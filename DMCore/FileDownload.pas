﻿unit FileDownload;

interface

uses
  Classes, SysUtils, FileWriter, WinInet, InetUtils, PartDownload, PartManager,
  PartInfo, States;

type
  TErrorMessage = (emGetFileInfo, emSaveFile, emCreatePath);
  TPartsDownload = array of TPartDownload;
  TPartsInfo = array of TPartInfo;

  TProcDownloadEnd = procedure(Sender: TObject; FileState: TFileState;
    ThreadID: Integer) of object;
  TProcDownloadGetInfo = procedure(Sender: TObject; ThreadID: Integer)
    of object;

  TFileDownload = class(TThread)
  private
    FThreadID: Integer;
    FFileSize: Int64;
    FFileName: string;
    FFileLink: string;
    FAcceptRanges: Boolean;
    FLastModified: string;
    FFileWriter: TFileWriter;
    FDownloadSpeed: Cardinal;
    FBytesDownloaded: Int64;
    FFileState: TFileState;
    FPartsInfo: TPartsInfo;
    FPartManager: TPartManager;
    FOnBufferDownload: TNotifyEvent;
    FOnDownloadEnd: TProcDownloadEnd;
    FErrorMessage: TErrorMessage;
    FOnDownloadStart: TNotifyEvent;
    FOnGetInfo: TProcDownloadGetInfo;
    FPartsCount: Integer;
    FRemainTime: string;
    FFilePath: string;
    FPartsDownloaded: Integer;
    FAddTime: TDateTime;
    FEndTime: TDateTime;
    procedure EventBufferDownload;
    procedure EventDownloadStart;
    procedure EventDownloadEnd(FileState: TFileState; ThreadID: Integer);
    procedure EventGetInfo(ThreadID: Integer);
    function GetFileInfo(var Size: Int64; var FileName: string): Boolean;
    procedure DownloadParts(FileSize: Int64);
    function InitDownload(var Parts: TPartsDownload): Boolean;
    function AllPartsComplete(Parts: TPartsDownload): Boolean;
    function GetPartsDownloaded(Parts: TPartsDownload): Integer;
    procedure FileChanged(Sender: TObject);
  protected
    procedure Execute; override;
  public
    constructor Create(FilePath: string; FileLink: string; ThreadID: Integer;
      PartsCount: Integer; FileName: string);

    procedure StopDownload;

    procedure SetInfo(FileName: string; FileSize, BytesDownloaded: Int64;
      FileState: TFileState; PartsDownloaded: Integer);

    property DownloadSpeed: Cardinal read FDownloadSpeed;
    property BytesDownloaded: Int64 read FBytesDownloaded
      write FBytesDownloaded;
    property ErrorMessage: TErrorMessage read FErrorMessage;
    property FileSize: Int64 read FFileSize;
    property FileName: string read FFileName;
    property FileState: TFileState read FFileState;
    property RemainTime: string read FRemainTime;
    property FilePath: string read FFilePath;
    property AddTime: TDateTime read FAddTime;
    property EndTime: TDateTime read FEndTime;

    property DownloadPath: string read FFilePath;
    property FileLink: string read FFileLink;
    property PartsCount: Integer read FPartsCount;
    property PartsDownloaded: Integer read FPartsDownloaded;

    property OnBufferDownload: TNotifyEvent read FOnBufferDownload
      write FOnBufferDownload;
    property OnDownloadEnd: TProcDownloadEnd read FOnDownloadEnd
      write FOnDownloadEnd;
    property OnDownloadStart: TNotifyEvent read FOnDownloadStart
      write FOnDownloadStart;
    property OnDownloadGetInfo: TProcDownloadGetInfo read FOnGetInfo
      write FOnGetInfo;

    property ThreadID: Integer read FThreadID write FThreadID;
  end;

implementation

{ TFileDownload }

procedure TFileDownload.EventBufferDownload;
begin
  if Assigned(FOnBufferDownload) then
    FOnBufferDownload(Self);
end;

procedure TFileDownload.EventDownloadEnd(FileState: TFileState;
  ThreadID: Integer);
begin
  if Assigned(FOnDownloadEnd) then
    FOnDownloadEnd(Self, FileState, ThreadID);
end;

procedure TFileDownload.EventDownloadStart;
begin
  if Assigned(FOnDownloadStart) then
    FOnDownloadStart(Self);
end;

procedure TFileDownload.EventGetInfo(ThreadID: Integer);
begin
  if Assigned(FOnGetInfo) then
    FOnGetInfo(Self, ThreadID);
end;

function TFileDownload.AllPartsComplete(Parts: TPartsDownload): Boolean;
var
  I: Integer;
begin
  Result := True;
  for I := Low(Parts) to High(Parts) do
    if Parts[I].PartState <> psCompleted then
    begin
      Result := False;
      Break;
    end;
end;

constructor TFileDownload.Create(FilePath: string; FileLink: string;
  ThreadID: Integer; PartsCount: Integer; FileName: string);
begin
  FFilePath := FilePath;
  FFileWriter := TFileWriter.Create(FFilePath);
  FFileLink := FileLink;
  FThreadID := ThreadID;
  FPartsCount := PartsCount;
  FFileName := FileName;

  FFileState := fsStopped;
  FAddTime := Now;

  FFileSize := 0;
  FDownloadSpeed := 0;
  FPartsDownloaded := 0;

  FreeOnTerminate := False;

  inherited Create(True);
end;

procedure TFileDownload.Execute;
begin
  inherited;
  FFileState := fsStopped;

  Synchronize(EventDownloadStart);

  // Создана ли папка для сохраненя фала
  if not FFileWriter.IsPath then
  begin
    FErrorMessage := emCreatePath;
    EventDownloadEnd(fsError, FThreadID);
    Exit;
  end;

  FFileState := fsGetInfo;

  // Получение информации о файле с сервера
  if GetFileInfo(FFileSize, FFileName) then
  begin
    EventGetInfo(FThreadID);
    // TODO: описание проблемы в процедуре
    // TODO: Если процедура не вызвана, освобождение FileWriter вызывает
    // исключение на закрытие файла.
    { if } FFileWriter.SetName(FFileName, False); { then }
    { begin }
    FFileState := fsDownloading;
    DownloadParts(FFileSize);
    FEndTime := Now;
    EventDownloadEnd(FFileState, FThreadID);
    { end }
    { else }
    { EventDownloadEnd(emSaveFile); }
  end
  else
  begin
    FEndTime := Now;
    FErrorMessage := emGetFileInfo;
    EventDownloadEnd(fsError, FThreadID);
  end;

  FFileWriter.Free;
  FDownloadSpeed := 0;
end;

function TFileDownload.InitDownload(var Parts: TPartsDownload): Boolean;
var
  I: Integer;
  PartSize, PartStart, PartCount, PartBytes: Int64;
  SizeLeft: Int64;
  InfoFileExists: Boolean;
begin
  Result := True;

  FBytesDownloaded := 0;

  FPartManager := TPartManager.Create(FFileName, FFileWriter.FilePath,
    FFileLink, FFileSize, FLastModified);
  FPartManager.OnFileChanged := FileChanged;

  if FAcceptRanges then
  begin
    SetLength(Parts, FPartsCount);
    SetLength(FPartsInfo, FPartsCount);

    // Если не удалось считать информацию из файла
    if not FPartManager.SetName(FFileName, InfoFileExists) then
    begin
      Result := False;
      Exit;
    end;

    // Забираем значения из класса который получило информацию о файле
    // Инициализируем части
    if InfoFileExists then
    begin
      FFileSize := FPartManager.FileSize;
      FBytesDownloaded := FPartManager.BytesDownloaded;

      for I := Low(Parts) to High(Parts) do
      begin
        PartStart := FPartManager.PartList[I].StartPosition;
        PartBytes := FPartManager.PartList[I].Count;

        Parts[I] := TPartDownload.Create(FFileLink, PartStart, PartBytes, I,
          FAcceptRanges);
      end;
    end
    else
    begin
      // Разбивание на части файла
      PartSize := FFileSize div FPartsCount;
      SizeLeft := FFileSize;

      for I := Low(Parts) to High(Parts) do
      begin
        if I <> High(Parts) then
        begin
          PartStart := PartSize * I;
          PartCount := PartSize;
        end
        else
        begin
          PartStart := FFileSize - SizeLeft;
          PartCount := SizeLeft;
        end;

        SizeLeft := SizeLeft - PartCount;

        Parts[I] := TPartDownload.Create(FFileLink, PartStart, PartCount, I,
          FAcceptRanges);
      end;
    end;
  end
  else
  begin
    // Если сервер не поддерживает докачку-начинаем сначала
    SetLength(Parts, 1);

    PartStart := 0;
    PartCount := FFileSize;
    Parts[0] := TPartDownload.Create(FFileLink, PartStart, PartCount, 0,
      FAcceptRanges);
  end;
end;

procedure TFileDownload.FileChanged(Sender: TObject);
begin

end;

procedure TFileDownload.DownloadParts(FileSize: Int64);
var
  I: Integer;
  Parts: TPartsDownload;
  BytesDownloaded: Int64;
  DownloadSpeed: Cardinal;
begin
  try
    // Инициализация потоков загрузки файла
    if not InitDownload(Parts) then
      FFileState := fsError;

    while FFileState = fsDownloading do
    begin
      DownloadSpeed := 0;

      BytesDownloaded := FPartManager.BytesDownloaded;

      if (FFileState = fsStopped) then
        Break;

      FPartsDownloaded := GetPartsDownloaded(Parts);

      for I := Low(Parts) to High(Parts) do
      begin

        Parts[I].WriteStream(FFileWriter.Stream);

        FPartsInfo[I].BytesDownloaded := Parts[I].BytesDownloaded;
        FPartsInfo[I].StartPosition := Parts[I].StartPos;
        FPartsInfo[I].Count := Parts[I].Count - Parts[I].BytesDownloaded;

        FPartManager.PartList.Add(FPartsInfo[I]);

        BytesDownloaded := BytesDownloaded + Parts[I].BytesDownloaded;
        DownloadSpeed := DownloadSpeed + Parts[I].DownloadSpeed;
      end;

      FBytesDownloaded := BytesDownloaded;
      FDownloadSpeed := DownloadSpeed;

      FPartManager.PartsDownloaded := FPartsDownloaded;
      FPartManager.WriteInfo;
      FPartManager.PartList.Clear;

      if FBytesDownloaded <> FPartManager.BytesDownloaded then
        Synchronize(EventBufferDownload);

      for I := Low(Parts) to High(Parts) do
        if Parts[I].PartState = psError then
        begin
          FFileState := fsError;
          Break;
        end;

      if AllPartsComplete(Parts) then
      begin
        FFileState := fsComplete;

        FPartManager.DeleteInfoFile;

        Break;
      end;

      Sleep(10);
    end;
  finally
    for I := Low(Parts) to High(Parts) do
      if Assigned(Parts[I]) then
        Parts[I].Free;

    FPartManager.Free;
  end;
end;

function TFileDownload.GetFileInfo(var Size: Int64;
  var FileName: string): Boolean;
var
  hInet, hOpen: HINTERNET;
  StatusCode: Boolean;
  Headers: string;
  dwTimeOut: Integer;
begin
  Result := False;
  dwTimeOut := 1000;

  hInet := InetOpen(UA_FIREFOX);
  FileName := InetGetFileName(hInet, FFileLink);
  try
    InternetSetOption(hInet, INTERNET_OPTION_CONNECT_TIMEOUT, @dwTimeOut,
      SizeOf(dwTimeOut));

    hOpen := InetOpenUrl(hInet, FFileLink, '');

    // Если соединение не устновлено
    if not Assigned(hOpen) then
      Exit;

    try
      Result := InetGetAllHeaders(hOpen, Headers);
      StatusCode := GetStatusCode(Headers);

      if StatusCode then
      begin
        GetContentLength(Headers, FFileSize);
        FAcceptRanges := GetAcceptRanges(Headers);
        GetLastModified(Headers, FLastModified);
      end;
    finally
      InternetCloseHandle(hOpen);
    end;
  finally
    InternetCloseHandle(hInet);
  end;
end;

function TFileDownload.GetPartsDownloaded(Parts: TPartsDownload): Integer;
var
  I: Integer;
  Count: Integer;
begin
  Count := 0;
  for I := Low(Parts) to High(Parts) do
  begin
    if Parts[I].PartState = psCompleted then
      Count := Count + 1;
  end;

  Result := Count;
end;

procedure TFileDownload.SetInfo(FileName: string;
  FileSize, BytesDownloaded: Int64; FileState: TFileState;
  PartsDownloaded: Integer);
begin
  FFileName := FileName;
  FFileSize := FileSize;
  FBytesDownloaded := BytesDownloaded;
  FFileState := FileState;
  FPartsDownloaded := PartsDownloaded;
end;

procedure TFileDownload.StopDownload;
begin
  if (FFileState <> fsComplete) and (FFileState <> fsError) then
    FFileState := fsStopped;

  FDownloadSpeed := 0;
end;

end.
