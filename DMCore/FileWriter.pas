﻿unit FileWriter;

interface

uses
  Classes, SysUtils;

type
  TFileWriter = class
  private
    FStream: TFileStream;
    FIsCreate: Boolean;
    FIsPathCreate: Boolean;
    FFilePath: string;
    FFileName: string;
  public
    constructor Create(Path: string);
    destructor Destroy; override;

    procedure SetName(Value: string; ReWrite: Boolean);

    property Stream: TFileStream read FStream;
    property IsPath: Boolean read FIsPathCreate;
    property FilePath: string read FFilePath;
  end;

implementation

{ TFileWriter }

constructor TFileWriter.Create(Path: string);
begin
  FIsCreate := False;
  FFilePath := Path;

  FIsPathCreate := DirectoryExists(Path) or ForceDirectories(Path);
  if not FIsPathCreate then
    RaiseLastOSError;
end;

destructor TFileWriter.Destroy;
begin
  FStream.Free;

  inherited;
end;

procedure TFileWriter.SetName(Value: string; ReWrite: Boolean);
var
  FileName: string;
  OpenType: Cardinal;
begin
  FFileName := Value;

  FileName := FFilePath + FFileName;
  if FileExists(FileName) and not ReWrite then
    OpenType := fmOpenReadWrite or fmShareDenyWrite
  else
    OpenType := fmCreate or fmOpenReadWrite or fmShareDenyWrite;

  //TODO: Может возникнуть проблем при записи файла. Реализовать обрботку ошибки
  // путем возврата результата выполенения оерации. По возможности вернуть описание
  // ошибки.
  FStream := TFileStream.Create(FileName, OpenType);
end;

end.
