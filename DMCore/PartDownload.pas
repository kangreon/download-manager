unit PartDownload;

interface

uses
  BufferUtils, PartDownloadThread, States, SysUtils, Classes;

type
  TPartDownload = class
  private
    FThread: TPartDownloadThread;
    FThreadNumber: Integer;
    function GetErrorDescription: string;
    function GetErrorMessage: TDownloadError;
    function GetPartState: TPartState;
    function GetSpeed: Cardinal;
    function GetCount: Int64;
    function GetStartPos: Integer;
    function GetBytesDownloaded: Int64;
  public
    constructor Create(FileLink: string; StartByte: Integer; Count: Int64;
      ThreadNumber: Integer; AcceptRanges: Boolean);
    destructor Destroy; override;

    procedure WriteStream(Stream: TStream);

    property DownloadSpeed: Cardinal read GetSpeed;
    property ErrorDescription: string read GetErrorDescription;
    property ErrorMessage: TDownloadError read GetErrorMessage;
    property PartState: TPartState read GetPartState;
    property BytesDownloaded: Int64 read GetBytesDownloaded;
    property Count: Int64 read GetCount;
    property StartPos: Integer read GetStartPos;
  end;

implementation

{ TPartDownload }

constructor TPartDownload.Create(FileLink: string; StartByte: Integer;
  Count: Int64; ThreadNumber: Integer; AcceptRanges: Boolean);
begin
  FThreadNumber := ThreadNumber;

  FThread := TPartDownloadThread.Create(FileLink, StartByte, Count,
    AcceptRanges);
  FThread.FreeOnTerminate := False;
  FThread.Start;
end;

destructor TPartDownload.Destroy;
begin
  FThread.StopDownload;
  while not FThread.IsTerminated do
    Sleep(30);

  FThread.Free;
  inherited;
end;

function TPartDownload.GetBytesDownloaded: Int64;
begin
  Result := FThread.BytesDownloaded;
end;

function TPartDownload.GetCount: Int64;
begin
  Result := FThread.Count;
end;

function TPartDownload.GetErrorDescription: string;
begin
  Result := FThread.ErrorDescription;
end;

function TPartDownload.GetErrorMessage: TDownloadError;
begin
  Result := FThread.ErrorMessage;
end;

function TPartDownload.GetPartState: TPartState;
begin
  Result := FThread.PartState;
end;

function TPartDownload.GetSpeed: Cardinal;
begin
  Result := FThread.DownloadSpeed;
end;

function TPartDownload.GetStartPos: Integer;
begin
  Result := FThread.StartByte;
end;

procedure TPartDownload.WriteStream(Stream: TStream);
begin
  if (FThread.PartState = psDownloading) or (FThread.PartState = psCompleted) or
    (FThread.PartState = psStopped) or (FThread.PartState = psError) then
    FThread.Buffer.WriteStream(Stream);
end;

end.
