﻿unit InetUtils;

interface

uses
  WinInet, SysUtils, Classes, Windows, StrUtils;

type
  PUrl = ^TUrl;

  TUrl = record
    Protocol: string;
    Host: string;
    Port: Word;
    Path: string;
    ExtraInfo: string;
  end;

procedure InetClose(hInet: HINTERNET);
function InetOpen(UserAgent: string): HINTERNET;
function InetOpenUrl(hInet: HINTERNET; Url: string; Headers: string): HINTERNET;
function InetContentEncoding(hInet: HINTERNET; out Text: string): Boolean;
function InetContentDisposition(hInet: HINTERNET; out Text: string): Boolean;
function InetStatusCode(hInet: HINTERNET; out StatusCode: Cardinal): Boolean;
function InetIsGzip(hInet: HINTERNET): Boolean;
function InetSize(hInet: HINTERNET; out Size: Int64): Boolean;
function InetGetFileName(hInet: HINTERNET; Link: string): string;
function InetValidateDownloadCode(StatusCode: Cardinal): Boolean;
function InetAcceptRanges(hInet: HINTERNET): Boolean;
function InetGetAllHeaders(hInet: HINTERNET; out Headers: string): Boolean;
function GetContentLength(Headers: string; out Size: Int64): Boolean;
function GetAcceptRanges(Headers: string): Boolean;
function GetStatusCode(Headers: string): Boolean;
function GetLastModified(Headers: string; out LastModified: string): Boolean;

function InetGetStream(hInet: HINTERNET; Stream: TStream; Link: string)
  : Boolean;
function InetSendPost(hInet: HINTERNET; Server, Resource: string;
  SendData: TStringList; Stream: TStream): Boolean;

function ParseURL(lpszUrl: string; Url: PUrl): Boolean;

const
  UA_FIREFOX
    : string =
    'Mozilla/5.0 (Windows NT 5.1; rv:24.0) Gecko/20100101 Firefox/24.0';

implementation

function InetValidateDownloadCode(StatusCode: Cardinal): Boolean;
begin
  Result := (StatusCode = HTTP_STATUS_OK) or
    (StatusCode = HTTP_STATUS_PARTIAL_CONTENT);
end;

procedure InetClose(hInet: HINTERNET);
begin
  if Assigned(hInet) then
    InternetCloseHandle(hInet);
end;

function InetOpen(UserAgent: string): HINTERNET;
var
  Flag: LongBool;
begin
  Result := InternetOpenW(PWideChar(UserAgent), INTERNET_OPEN_TYPE_DIRECT,
    nil, nil, 0);

  Flag := True;
  InternetSetOptionA(Result, INTERNET_OPTION_HTTP_DECODING, PAnsiChar(@Flag),
    SizeOf(Flag))
end;

function InetOpenUrl(hInet: HINTERNET; Url: string; Headers: string): HINTERNET;
begin
  Result := InternetOpenUrl(hInet, PWideChar(Url), PWideChar(Headers),
    Length(Headers), INTERNET_FLAG_RELOAD, 0);
end;

function InetQueryInfo(hInet: HINTERNET; InfoLevel: Cardinal;
  out Text: string): Boolean;
var
  Buffer: array [0 .. 200] of AnsiChar;
  BufferLength, Reserved: Cardinal;
begin
  BufferLength := Length(Buffer);
  Reserved := 0;

  Result := HttpQueryInfoA(hInet, InfoLevel, @Buffer, BufferLength, Reserved);

  if Result then
    Text := string(Buffer);
end;

function InetContentEncoding(hInet: HINTERNET; out Text: string): Boolean;
begin
  Result := InetQueryInfo(hInet, HTTP_QUERY_CONTENT_ENCODING, Text);
end;

function InetContentDisposition(hInet: HINTERNET; out Text: string): Boolean;
begin
  Result := InetQueryInfo(hInet, HTTP_QUERY_CONTENT_DISPOSITION, Text);
end;

function InetGetFileName(hInet: HINTERNET; Link: string): string;
var
  i: Cardinal;
  IsCopyFromLink: Boolean;
begin
  IsCopyFromLink := True;

  // http://tools.ietf.org/html/rfc2183#section-2.3

  if IsCopyFromLink then
  begin
    i := 1;
    while Pos('/', Link, i) <> 0 do
      i := Pos('/', Link, i) + 1;

    Result := Copy(Link, i, Length(Link));
  end
  else
    Result := 'default';
end;

function InetStatusCode(hInet: HINTERNET; out StatusCode: Cardinal): Boolean;
var
  BufferCode, Reserved: Cardinal;
begin
  BufferCode := SizeOf(StatusCode);
  Reserved := 0;

  Result := HttpQueryInfo(hInet, HTTP_QUERY_STATUS_CODE or
    HTTP_QUERY_FLAG_NUMBER, @StatusCode, BufferCode, Reserved);
end;

function InetIsGzip(hInet: HINTERNET): Boolean;
var
  s: string;
begin
  Result := False;

  if InetContentEncoding(hInet, s) then
    Result := Pos('gzip', AnsiLowerCase(s)) <> 0;
end;

function InetAcceptRanges(hInet: HINTERNET): Boolean;
var
  res: string;
  BufferLength, Reserved: Cardinal;
begin
  SetLength(res, 6);
  Reserved := 0;
  BufferLength := Length(res);

  if HttpQueryInfoA(hInet, HTTP_QUERY_ACCEPT_RANGES, PChar(res), BufferLength,
    Reserved) then
    Result := Pos('bytes', AnsiLowerCase(res)) <> 0
  else
    Result := False;
end;

function InetGetAllHeaders(hInet: HINTERNET; out Headers: string): Boolean;
var
  s: string;
  BufferLength, Reserved: Cardinal;
begin
  SetLength(s, 8);
  BufferLength := Length(s);
  Reserved := 0;

  Result := False;

  if HttpQueryInfo(hInet, HTTP_QUERY_RAW_HEADERS_CRLF, PChar(s), BufferLength,
    Reserved) then
  begin
    Headers := s;
    Result := True;
  end
  else if GetLastError = ERROR_INSUFFICIENT_BUFFER then // увеличиваем буффер
  begin
    SetLength(s, BufferLength);

    if HttpQueryInfo(hInet, HTTP_QUERY_RAW_HEADERS_CRLF, PChar(s), BufferLength,
      Reserved) then
    begin
      Headers := s;
      Result := True;
    end;
  end;
end;

function GetContentLength(Headers: string; out Size: Int64): Boolean;
const
  str = 'content-length';
  Del = #$D#$A;
var
  Hdrs: string;
  P: Integer;
  P1: Integer;
  Count: Integer;
begin
  Result := False;

  Hdrs := LowerCase(Headers);
  P := Pos(str, Hdrs);

  if P <> 0 then
  begin
    P1 := PosEx(Del, Hdrs, P);

    if P1 <> 0 then
    begin
      Count := P1 - (P + Length(str) + 2);
      Size := StrToInt(System.Copy(Hdrs, P + Length(str) + 2, Count));
      Result := True;
    end;
  end;
end;

function GetAcceptRanges(Headers: string): Boolean;
const
  str = 'accept-ranges';
  Del = #$D#$A;
var
  Hdrs: string;
  P: Integer;
  P1: Integer;
  Count: Integer;
  s: string;
begin
  Hdrs := LowerCase(Headers);
  P := Pos(str, Hdrs);

  if P <> 0 then
  begin
    P1 := PosEx(Del, Hdrs, P);

    if P1 <> 0 then
    begin
      Count := P1 - (P + Length(str) + 2);
      s := System.Copy(Hdrs, P + Length(str) + 2, Count);
    end;
  end;

  if s = 'bytes' then
    Result := True
  else
    Result := False;
end;

function GetStatusCode(Headers: string): Boolean;
const
  str = '200 ok';
var
  Hdrs: string;
  P: Integer;
begin
  Result := False;

  Hdrs := LowerCase(Headers);

  P := Pos(str, Hdrs);
  if P <> 0 then
    Result := True;
end;

function GetLastModified(Headers: string; out LastModified: string): Boolean;
const
  str = 'last-modified';
  Del = #$D#$A;
var
  Hdrs: string;
  P: Integer;
  P1: Integer;
  Count: Integer;
  s: string;
begin
  s := '';

  Hdrs := LowerCase(Headers);
  P := Pos(str, Hdrs);

  if P <> 0 then
  begin
    P1 := PosEx(Del, Hdrs, P);

    if P1 <> 0 then
    begin
      Count := P1 - (P + Length(str) + 2);
      s := System.Copy(Hdrs, P + Length(str) + 2, Count);
    end;
  end;

  if s <> '' then
  begin
    Result := True;
    LastModified := s;
  end
  else
    Result := False;
end;

function InetSize(hInet: HINTERNET; out Size: Int64): Boolean;
var
  BufferLength, Reserved: Cardinal;
  Error: Cardinal;
begin
  Reserved := 0;
  BufferLength := SizeOf(Size);

  Result := HttpQueryInfoA(hInet, HTTP_QUERY_CONTENT_LENGTH or
    HTTP_QUERY_FLAG_NUMBER64, PByte(@Size), BufferLength, Reserved);

  Error := GetLastError;

  if Error = ERROR_INSUFFICIENT_BUFFER then
    Result := HttpQueryInfoA(hInet, HTTP_QUERY_CONTENT_LENGTH or
      HTTP_QUERY_FLAG_NUMBER64, PByte(@Size), BufferLength, Reserved);
end;

function InetGetStream(hInet: HINTERNET; Stream: TStream; Link: string)
  : Boolean;
const
  HEADER: string = 'Accept-Encoding: identity'#13#10;
  BUFFER_COUNT = 1024;
var
  hReq: HINTERNET;
  StatusCode: Cardinal;
  Buffer: array [0 .. BUFFER_COUNT] of Byte;
  BufferPtr: Pointer;
  BufferSize, BufferRead: Cardinal;
  IsError: Boolean;
begin
  Result := False;

  hReq := InternetOpenUrl(hInet, PWideChar(Link), PWideChar(HEADER),
    Length(HEADER), INTERNET_FLAG_KEEP_CONNECTION or INTERNET_FLAG_RELOAD, 0);
  if Assigned(hReq) then
  begin
    try
      if not InetStatusCode(hReq, StatusCode) then
        Exit;

      if HTTP_STATUS_OK <> StatusCode then
        Exit;

      BufferPtr := @Buffer;
      BufferSize := Length(Buffer);

      repeat
        BufferRead := 0;
        IsError := False;

        if InternetReadFile(hReq, BufferPtr, BufferSize, BufferRead) then
        begin
          Stream.Write(BufferPtr^, BufferRead);
        end
        else
        begin
          IsError := True;
          Break;
        end;

      until BufferRead = 0;

      Result := not IsError;
    finally
      InternetCloseHandle(hReq);
    end;
  end;
end;

function InetSendPost(hInet: HINTERNET; Server, Resource: string;
  SendData: TStringList; Stream: TStream): Boolean;
const
  ACCEPT: packed array [0 .. 1] of PWideChar = (PWideChar('*/*'), nil);
  HEADER: string = 'Content-Type: application/x-www-form-urlencoded'#13#10;
  BUFFER_COUNT = 1024;

var
  Data: string;
  hConnect, hRequest: HINTERNET;
  code: Cardinal;
  Buffer: array [0 .. BUFFER_COUNT] of Byte;
  BufferPtr: Pointer;
  BufferSize, BufferRead: Cardinal;
  IsError: Boolean;
begin
  Result := False;
  Data := StringReplace(SendData.Text, #13#10, '&', [rfReplaceAll]);

  hConnect := InternetConnect(hInet, PWideChar(Server), 80, nil, nil,
    INTERNET_SERVICE_HTTP, 0, 1);
  if Assigned(hConnect) then
  begin
    try
      hRequest := HttpOpenRequest(hConnect, 'POST', PWideChar(Resource), '1.1',
        '', @ACCEPT, 0, 1);
      if Assigned(hRequest) then
      begin
        try
          if not HttpSendRequestA(hRequest, PAnsiChar(AnsiString(HEADER)),
            Length(HEADER), PAnsiChar(AnsiString(Data)), Length(Data)) then
            Exit;

          if not InetStatusCode(hRequest, code) then
            Exit;
          if code <> HTTP_STATUS_OK then
            Exit;

          BufferPtr := @Buffer;
          BufferSize := Length(Buffer);

          repeat
            BufferRead := 0;
            IsError := False;

            if InternetReadFile(hRequest, BufferPtr, BufferSize, BufferRead)
            then
            begin
              Stream.Write(BufferPtr^, BufferRead);
            end
            else
            begin
              IsError := True;
              Break;
            end;

          until BufferRead = 0;

          Result := not IsError;

        finally
          InternetCloseHandle(hRequest);
        end;
      end;
    finally
      InternetCloseHandle(hConnect);
    end;
  end;
end;

function ParseURL(lpszUrl: string; Url: PUrl): Boolean;
var
  lpszScheme: array [0 .. INTERNET_MAX_SCHEME_LENGTH - 1] of Char;
  lpszHostName: array [0 .. INTERNET_MAX_HOST_NAME_LENGTH - 1] of Char;
  lpszUserName: array [0 .. INTERNET_MAX_USER_NAME_LENGTH - 1] of Char;
  lpszPassword: array [0 .. INTERNET_MAX_PASSWORD_LENGTH - 1] of Char;
  lpszUrlPath: array [0 .. INTERNET_MAX_PATH_LENGTH - 1] of Char;
  lpszExtraInfo: array [0 .. 1024 - 1] of Char;
  lpUrlComponents: TURLComponents;
begin
  ZeroMemory(@lpszScheme, SizeOf(lpszScheme));
  ZeroMemory(@lpszHostName, SizeOf(lpszHostName));
  ZeroMemory(@lpszUserName, SizeOf(lpszUserName));
  ZeroMemory(@lpszPassword, SizeOf(lpszPassword));
  ZeroMemory(@lpszUrlPath, SizeOf(lpszUrlPath));
  ZeroMemory(@lpszExtraInfo, SizeOf(lpszExtraInfo));
  ZeroMemory(@lpUrlComponents, SizeOf(TURLComponents));

  lpUrlComponents.dwStructSize := SizeOf(TURLComponents);
  lpUrlComponents.lpszScheme := lpszScheme;
  lpUrlComponents.dwSchemeLength := SizeOf(lpszScheme);
  lpUrlComponents.lpszHostName := lpszHostName;
  lpUrlComponents.dwHostNameLength := SizeOf(lpszHostName);
  lpUrlComponents.lpszUserName := lpszUserName;
  lpUrlComponents.dwUserNameLength := SizeOf(lpszUserName);
  lpUrlComponents.lpszPassword := lpszPassword;
  lpUrlComponents.dwPasswordLength := SizeOf(lpszPassword);
  lpUrlComponents.lpszUrlPath := lpszUrlPath;
  lpUrlComponents.dwUrlPathLength := SizeOf(lpszUrlPath);
  lpUrlComponents.lpszExtraInfo := lpszExtraInfo;
  lpUrlComponents.dwExtraInfoLength := SizeOf(lpszExtraInfo);

  Result := InternetCrackUrl(PChar(lpszUrl), Length(lpszUrl),
    ICU_DECODE or ICU_ESCAPE, lpUrlComponents);
  if Result then
  begin
    Url^.Protocol := StrPas(lpszScheme);
    Url^.Host := StrPas(lpszHostName);
    Url^.Port := lpUrlComponents.nPort;
    Url^.Path := StrPas(lpszUrlPath);
    Url^.ExtraInfo := StrPas(lpszExtraInfo);
  end;
end;

end.
