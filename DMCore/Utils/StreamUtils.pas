unit StreamUtils;

interface

uses
  Classes, SysUtils, Types;

type
  TAnsiStringDynArray = array of AnsiString;

  TStreamUtils = class
  private
    FStream: TStream;
  public
    constructor Create(Stream: TStream);

    procedure Clear;
    procedure FirstByte;

    function Read(var Buffer: TBytes; Count: Longint): Longint;
    function ReadInteger(var Value: Integer): Boolean;
    function ReadInt64(var Value: Int64): Boolean;
    function ReadCardinal(var Value: Cardinal): Boolean;
    function ReadString(var Value: WideString): Boolean;
    function ReadStringS(var Value: string): Boolean;
    function ReadAnsiString(var Value: AnsiString): Boolean;
    function ReadArrayInteger(var Value: TIntegerDynArray): Boolean;
    function ReadArrayString(var Value: TWideStringDynArray): Boolean;
    function ReadArrayAnsiString(out Value: TAnsiStringDynArray): Boolean;
    function ReadWord(out Value: Word): Boolean;
    function ReadDouble(out Value: Double): Boolean;
    function ReadByte(out Value: Byte): Boolean;
    function ReadBool(out Value: Boolean): Boolean;

    function Write(const Buffer; Count: Longint): Longint;
    function WriteInteger(Value: Integer): Boolean;
    function WriteInt64(Value: Int64): Boolean;
    function WriteCardinal(Value: Cardinal): Boolean;
    function WriteString(Value: WideString): Boolean;
    function WriteAnsiString(Value: AnsiString): Boolean;
    function WriteArrayInteger(Value: TIntegerDynArray): Boolean;
    function WriteArrayString(Value: TWideStringDynArray): Boolean;
    function WriteArrayAnsiString(Value: TAnsiStringDynArray): Boolean;
    function WriteWord(Value: Word): Boolean;
    function WriteDouble(Value: Double): Boolean;
    function WriteByte(Value: Byte): Boolean;
    function WriteBool(Value: Boolean): Boolean;
  end;

  TStreamUtilsEx = class(TStreamUtils)
  private
    FStream: TFileStream;
  public
    constructor Create(FileName: string);
    destructor Destroy; override;
  end;

implementation

{ TStreamUtils }

procedure TStreamUtils.Clear;
begin
  FStream.Size := 0;
  FStream.Position := 0;
end;

constructor TStreamUtils.Create(Stream: TStream);
begin
  FStream := Stream;
end;

procedure TStreamUtils.FirstByte;
begin
  FStream.Position := 0;
end;

function TStreamUtils.Read(var Buffer: TBytes; Count: Integer): Longint;
begin
  Result := FStream.Read(Buffer, Count);
end;

function TStreamUtils.ReadAnsiString(var Value: AnsiString): Boolean;
var
  StrSize: Integer;
  ReadSize: Integer;
begin
  if ReadInteger(StrSize) then
  begin
    if FStream.Position + StrSize <= FStream.Size then
    begin
      if StrSize > 0 then
      begin
        SetLength(Value, StrSize);
        ReadSize := FStream.Read(Value[1], StrSize);
        Result := ReadSize = StrSize;
      end
      else if StrSize = 0 then
      begin
        Value := '';
        Result := True;
      end
      else
        Result := False;
    end
    else
      Result := False;
  end
  else
    Result := False;
end;

function TStreamUtils.ReadArrayAnsiString(
  out Value: TAnsiStringDynArray): Boolean;
var
  i: Integer;
begin
  Result := ReadInteger(i);
  if Result then
  begin
    SetLength(Value, i);

    for i := Low(Value) to High(Value) do
      if Result then
      begin
        Result := Result and ReadAnsiString(Value[i]);
      end;
  end;
end;

function TStreamUtils.ReadArrayInteger(var Value: TIntegerDynArray): Boolean;
var
  i: Integer;
begin
  Result := ReadInteger(i);
  if Result then
  begin
    SetLength(Value, i);

    for i := Low(Value) to High(Value) do
      if Result then
        Result := Result and ReadInteger(Value[i]);
  end;
end;

function TStreamUtils.ReadArrayString(var Value: TWideStringDynArray): Boolean;
var
  i: Integer;
begin
  Result := ReadInteger(i);
  if Result then
  begin
    SetLength(Value, i);

    for i := Low(Value) to High(Value) do
      if Result then
        Result := Result and ReadString(Value[i]);
  end;
end;

function TStreamUtils.ReadBool(out Value: Boolean): Boolean;
var
  b: Byte;
begin
  Result := ReadByte(b);
  Value := b <> 0;
end;

function TStreamUtils.ReadByte(out Value: Byte): Boolean;
var
  ReadSize: Integer;
begin
  if FStream.Position + SizeOf(Value) <= FStream.Size then
  begin
    ReadSize := FStream.Read(Value, SizeOf(Value));
    Result := ReadSize = SizeOf(Value);
  end
  else
    Result := False;
end;

function TStreamUtils.ReadCardinal(var Value: Cardinal): Boolean;
var
  ReadSize: Integer;
begin
  if FStream.Position + SizeOf(Value) <= FStream.Size then
  begin
    ReadSize := FStream.Read(Value, SizeOf(Value));
    Result := ReadSize = SizeOf(Value);
  end
  else
    Result := False;
end;

function TStreamUtils.ReadDouble(out Value: Double): Boolean;
var
  ReadSize: Integer;
begin
  if FStream.Position + SizeOf(Value) <= FStream.Size then
  begin
    ReadSize := FStream.Read(Value, SizeOf(Value));
    Result := ReadSize = SizeOf(Value);
  end
  else
    Result := False;
end;

function TStreamUtils.ReadInt64(var Value: Int64): Boolean;
var
  ReadSize: Integer;
begin
  if FStream.Position + SizeOf(Value) <= FStream.Size then
  begin
    ReadSize := FStream.Read(Value, SizeOf(Value));
    Result := ReadSize = SizeOf(Value);
  end
  else
    Result := False;
end;

function TStreamUtils.ReadInteger(var Value: Integer): Boolean;
var
  ReadSize: Integer;
begin
  if FStream.Position + SizeOf(Value) <= FStream.Size then
  begin
    ReadSize := FStream.Read(Value, SizeOf(Value));
    Result := ReadSize = SizeOf(Value);
  end
  else
    Result := False;
end;

function TStreamUtils.ReadString(var Value: WideString): Boolean;
var
  StrSize: Integer;
  ReadSize: Integer;
begin
  if ReadInteger(StrSize) then
  begin
    if FStream.Position + StrSize <= FStream.Size then
    begin
      if StrSize > 0 then
      begin
        SetLength(Value, StrSize div SizeOf(Char));
        ReadSize := FStream.Read(Value[1], StrSize);
        Result := ReadSize = StrSize;
      end
      else if StrSize = 0 then
      begin
        Value := '';
        Result := True;
      end
      else
        Result := False;
    end
    else
      Result := False;
  end
  else
    Result := False;
end;

function TStreamUtils.ReadStringS(var Value: string): Boolean;
var
  s: WideString;
begin
  Result := ReadString(s);
  if Result then
    Value := s;
end;

function TStreamUtils.ReadWord(out Value: Word): Boolean;
var
  ReadSize: Integer;
begin
  if FStream.Position + SizeOf(Value) <= FStream.Size then
  begin
    ReadSize := FStream.Read(Value, SizeOf(Value));
    Result := ReadSize = SizeOf(Value);
  end
  else
    Result := False;
end;

function TStreamUtils.Write(const Buffer; Count: Integer): Longint;
begin
  Result := FStream.Write(Buffer, Count);
end;

function TStreamUtils.WriteAnsiString(Value: AnsiString): Boolean;
var
  TextSize: Integer;
  WriteSize: Integer;
begin
  TextSize := Length(Value);

  if WriteInteger(TextSize) then
  begin
    if TextSize > 0 then
    begin
      WriteSize := FStream.Write(Value[1], TextSize);
      Result := WriteSize = TextSize;
    end
    else
      Result := True;
  end
  else
    Result := False;
end;

function TStreamUtils.WriteArrayAnsiString(Value: TAnsiStringDynArray): Boolean;
var
  i: Integer;
  a: AnsiString;
begin
  Result := WriteInteger(Length(Value));
  for i := Low(Value) to High(Value) do
    if Result then
    begin
      a := Value[i];
      Result := Result and WriteAnsiString(a);
    end;
end;

function TStreamUtils.WriteArrayInteger(Value: TIntegerDynArray): Boolean;
var
  i: Integer;
begin
  Result := WriteInteger(Length(Value));
  for i := Low(Value) to High(Value) do
    if Result then
      Result := Result and WriteInteger(Value[i]);
end;

function TStreamUtils.WriteArrayString(Value: TWideStringDynArray): Boolean;
var
  i: Integer;
begin
  Result := WriteInteger(Length(Value));
  for i := Low(Value) to High(Value) do
    if Result then
      Result := Result and WriteString(Value[i]);
end;

function TStreamUtils.WriteBool(Value: Boolean): Boolean;
var
  b: Byte;
begin
  if Value then
    b := 1
  else
    b := 0;

  Result := WriteByte(b);
end;

function TStreamUtils.WriteByte(Value: Byte): Boolean;
var
  WriteSize: Integer;
begin
  WriteSize := FStream.Write(Value, SizeOf(Value));
  Result := WriteSize = SizeOf(Value);
end;

function TStreamUtils.WriteCardinal(Value: Cardinal): Boolean;
var
  WriteSize: Integer;
begin
  WriteSize := FStream.Write(Value, SizeOf(Value));
  Result := WriteSize = SizeOf(Value);
end;

function TStreamUtils.WriteDouble(Value: Double): Boolean;
var
  WriteSize: Integer;
begin
  WriteSize := FStream.Write(Value, SizeOf(Value));
  Result := WriteSize = SizeOf(Value);
end;

function TStreamUtils.WriteInt64(Value: Int64): Boolean;
var
  WriteSize: Integer;
begin
  WriteSize := FStream.Write(Value, SizeOf(Value));
  Result := WriteSize = SizeOf(Value);
end;

function TStreamUtils.WriteInteger(Value: Integer): Boolean;
var
  WriteSize: Integer;
begin
  WriteSize := FStream.Write(Value, SizeOf(Value));
  Result := WriteSize = SizeOf(Value);
end;

function TStreamUtils.WriteString(Value: WideString): Boolean;
var
  TextSize: Integer;
  WriteSize: Integer;
begin
  TextSize := Length(Value) * SizeOf(Char);

  if WriteInteger(TextSize) then
  begin
    if TextSize > 0 then
    begin
      WriteSize := FStream.Write(Value[1], TextSize);
      Result := WriteSize = TextSize;
    end
    else
      Result := True;
  end
  else
    Result := False;
end;

function TStreamUtils.WriteWord(Value: Word): Boolean;
var
  WriteSize: Integer;
begin
  WriteSize := FStream.Write(Value, SizeOf(Value));
  Result := WriteSize = SizeOf(Value);
end;

{ TStreamUtilsEx }

constructor TStreamUtilsEx.Create(FileName: string);
var
  OpenMode: Cardinal;
begin
  if FileExists(FileName) then
    OpenMode := fmOpenReadWrite or fmShareDenyWrite
  else
    OpenMode := fmCreate or fmOpenReadWrite or fmShareDenyWrite;

  FStream := TFileStream.Create(FileName, OpenMode);
  inherited Create(FStream);
end;

destructor TStreamUtilsEx.Destroy;
begin
  FStream.Free;
  inherited;
end;

end.
