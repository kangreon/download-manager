unit PartInfo;

interface

uses
  Math, Types;

type
  TPartInfo = record
  private
    function GetEndPosition: Int64;
    procedure SetEndPosition(const Value: Int64);

  public
    StartPosition: Int64;
    Count: Int64;
    BytesDownloaded: Int64;

    function Merge(PartInfo: TPartInfo): TPartInfo;

    function Contains(PartInfo: TPartInfo): Boolean;
    property EndPosition: Int64 read GetEndPosition write SetEndPosition;
  end;

implementation

{ TPartInfo }

{ *  ������������ �� ���������� � ����� �� �� ���������� � ����
  * }
function TPartInfo.Contains(PartInfo: TPartInfo): Boolean;
begin
  Result := ((Self.StartPosition <= PartInfo.EndPosition + 1) and
    (Self.EndPosition + 1 >= PartInfo.StartPosition))
end;

function TPartInfo.GetEndPosition: Int64;
begin
  Result := Self.StartPosition + Self.Count + 1;
end;

procedure TPartInfo.SetEndPosition(const Value: Int64);
begin
  Self.Count := Value - (Self.StartPosition + 1)
end;

function TPartInfo.Merge(PartInfo: TPartInfo): TPartInfo;
begin
  Result.StartPosition := Min(Self.StartPosition, PartInfo.StartPosition);
  Result.EndPosition := Max(Self.EndPosition, PartInfo.EndPosition);
end;

end.
