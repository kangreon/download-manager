﻿unit PartDownloadThread;

interface

uses
  InetUtils, WinInet, Classes, SysUtils, BufferUtils, Windows, DateUtils,
  States;

type
  TPartDownloadThread = class(TThread)
  private
    FStartByte: Int64;
    FCount: Int64;
    FFileLink: string;
    FIsStopDownload: Boolean;
    FBuffer: TBufferUtils;
    FPartState: TPartState;
    FStartTime: TDateTime;
    FSpeedBytesCount: Cardinal;
    FSpeed: array [0 .. 4] of Cardinal;
    FSpeedPosition: Cardinal;
    FErrorMessage: TDownloadError;
    FErrorDescription: string;
    FBytesDownloaded: Int64;
    FIsTerminated: Boolean;
    FAcceptRanges: Boolean;
    procedure InsertSpeedValue(BytesCount: Cardinal);
    procedure InsertSpeed(Speed: Cardinal);
    function GetSpeed: Cardinal;
    procedure DownloadRange(hOpen: HINTERNET);
    procedure SetErrorMessage(Value: TDownloadError); overload;
    procedure SetErrorMessage(Value: TDownloadError;
      Description: string); overload;
    procedure SetErrorMessage(Value: TDownloadError;
      Description: Integer); overload;
  protected
    procedure Execute; override;
  public
    constructor Create(FileLink: string; StartByte, Count: Int64;
      AcceptRanges: Boolean);

    procedure StopDownload;

    property IsTerminated: Boolean read FIsTerminated;
    property DownloadSpeed: Cardinal read GetSpeed;
    property BytesDownloaded: Int64 read FBytesDownloaded;
    property ErrorDescription: string read FErrorDescription;
    property ErrorMessage: TDownloadError read FErrorMessage;
    property PartState: TPartState read FPartState;
    property StartByte: Int64 read FStartByte;
    property Count: Int64 read FCount;

    property Buffer: TBufferUtils read FBuffer;
  end;

const
  BUFFER_SIZE = 1024;

implementation

{ TPartDownload }

constructor TPartDownloadThread.Create(FileLink: string;
  StartByte, Count: Int64; AcceptRanges: Boolean);
begin
  FFileLink := FileLink;
  FStartByte := StartByte;
  FCount := Count;
  FBytesDownloaded := 0;
  FAcceptRanges := AcceptRanges;

  FIsStopDownload := False;
  FIsTerminated := False;

  FPartState := psConnecting;
  inherited Create(True);
end;

procedure TPartDownloadThread.Execute;
const
  HEADER_RANGE: string = 'Range: bytes=%d-';

var
  hInet, hOpen: HINTERNET;
  HeaderRange: string;
  StatusCode: Cardinal;
  dwTimeOut: Cardinal;
begin
  inherited;

  dwTimeOut := 1000;

  if FAcceptRanges then
  begin
    if FStartByte > 0 then
      HeaderRange := Format(HEADER_RANGE, [FStartByte]);
  end
  else
    HeaderRange := '';

  hInet := InetOpen(UA_FIREFOX);
  try
    InternetSetOption(hInet, INTERNET_OPTION_CONNECT_TIMEOUT, @dwTimeOut,
      SizeOf(dwTimeOut));

    repeat
      hOpen := InetOpenUrl(hInet, FFileLink, HeaderRange);

      // В случае остановки скачивания, закрыть соединение с сервером
      if FIsStopDownload then
      begin
        InetClose(hOpen);
        Break;
      end;
    until Assigned(hOpen);

    if not Assigned(hOpen) then
      Exit;

    try
      FPartState := psConnected;

      if InetStatusCode(hOpen, StatusCode) then
      begin
        if InetValidateDownloadCode(StatusCode) then
          DownloadRange(hOpen)
        else
          SetErrorMessage(deCodeFailed, StatusCode);
      end
      else
        SetErrorMessage(deGetCode);

    finally
      InetClose(hOpen);
    end;
  finally
    InetClose(hInet);
    FIsTerminated := True;
  end;
end;

procedure TPartDownloadThread.SetErrorMessage(Value: TDownloadError);
begin
  SetErrorMessage(Value, '');
end;

procedure TPartDownloadThread.SetErrorMessage(Value: TDownloadError;
  Description: string);
begin
  FPartState := psError;
  FErrorMessage := Value;
  FErrorDescription := Description;
end;

procedure TPartDownloadThread.SetErrorMessage(Value: TDownloadError;
  Description: Integer);
begin
  SetErrorMessage(Value, IntToStr(Description));
end;

{ *  Останавливает скачивание части максимально быстро
  * }
procedure TPartDownloadThread.StopDownload;
begin
  FIsStopDownload := True;
end;

{ *  Процедура скачивания файла с сервера. Записывает скачанные данные во
  *  временный буфер без ожидания чтения данных из него.
  * }
procedure TPartDownloadThread.DownloadRange(hOpen: HINTERNET);
var
  BytePosition: Integer;
  Buffer: array [0 .. BUFFER_SIZE] of Byte;
  BufferPtr: Pointer;
  BufferSize, BufferRead: Cardinal;
begin
  FBuffer := TBufferUtils.Create(BUFFER_SIZE, 5);

  FPartState := psDownloading;
  InsertSpeedValue(0);

  BytePosition := FStartByte;
  BufferPtr := @Buffer;

  repeat
    BufferSize := FCount - FBytesDownloaded;
    if BufferSize > BUFFER_SIZE then
      BufferSize := BUFFER_SIZE;

    if BufferSize <= 0 then
    begin
      FPartState := psCompleted;
      Break;
    end;

    // Скачивание остановлено
    if FIsStopDownload then
    begin
      FPartState := psStopped;
      Break;
    end;

    if InternetReadFile(hOpen, BufferPtr, BufferSize, BufferRead) then
    begin
      if FIsStopDownload then
      begin
        FPartState := psStopped;
        Break;
      end;

      while not FBuffer.InsertBuffer(BufferPtr, BytePosition, BufferRead) do
      begin
        if FIsStopDownload then
        begin
          FPartState := psStopped;
          Break;
        end;
      end;

      if FIsStopDownload then
      begin
        FPartState := psStopped;
        Break;
      end;

      FBytesDownloaded := FBytesDownloaded + BufferRead;
      BytePosition := BytePosition + Integer(BufferRead);

      InsertSpeedValue(BufferRead);
    end
    else
    begin
      SetErrorMessage(deInetReadFile);
      Break;
    end;

  until BufferRead = 0;

  if FPartState = psDownloading then
    FPartState := psCompleted;
end;

function TPartDownloadThread.GetSpeed: Cardinal;
var
  I: Integer;
  Sum, dc: Cardinal;
begin
  if FPartState <> psDownloading then
    Exit(0);

  dc := 0;
  Sum := 0;

  for I := Low(FSpeed) to High(FSpeed) do
  begin
    if FSpeed[I] > 0 then
    begin
      Sum := Sum + FSpeed[I];
      dc := dc + 1;
    end;
  end;

  if dc > 0 then
    Result := Sum div dc
  else
    Result := 0;
end;

procedure TPartDownloadThread.InsertSpeed(Speed: Cardinal);
begin
  if (FSpeedPosition > High(FSpeed)) then
    FSpeedPosition := Low(FSpeed);

  FSpeed[FSpeedPosition] := Speed;
  FSpeedPosition := FSpeedPosition + 1;
end;

procedure TPartDownloadThread.InsertSpeedValue(BytesCount: Cardinal);
var
  DownloadTime: TDateTime;
begin
  if BytesCount = 0 then
  begin
    FStartTime := Now;
    FSpeedBytesCount := 0;
  end
  else
  begin
    DownloadTime := Now - FStartTime;
    FSpeedBytesCount := FSpeedBytesCount + BytesCount;

    if DownloadTime > OneSecond then
    begin
      InsertSpeed(Round(FSpeedBytesCount * OneSecond / DownloadTime));
      InsertSpeedValue(0);
    end;
  end;
end;

end.
