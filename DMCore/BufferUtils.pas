﻿unit BufferUtils;

interface

uses
  Classes, SysUtils;

type
  TBufferUtils = class
  private
    FPosition: array of Int64;
    FSize: array of Int64;
    FIsFree: array of Boolean;
    FBuffers: array of PByte;
    function WaitFreeBuffer: Integer;
  public
    constructor Create(BufferSize, BufferCount: Cardinal);
    destructor Destroy; override;

    function InsertBuffer(Data: PByte; Position, Size: Int64): Boolean;
    function WriteStream(Stream: TStream): Int64;
  end;

implementation

{ TBufferUtils }

constructor TBufferUtils.Create(BufferSize, BufferCount: Cardinal);
var
  i: Integer;
begin
  SetLength(FBuffers, BufferCount);
  SetLength(FPosition, BufferCount);
  SetLength(FSize, BufferCount);
  SetLength(FIsFree, BufferCount);

  for i := Low(FBuffers) to High(FBuffers) do
  begin
    GetMem(FBuffers[i], BufferSize);
    FIsFree[i] := True;
  end;
end;

destructor TBufferUtils.Destroy;
var
  i: Integer;
begin
  for i := Low(FBuffers) to High(FBuffers) do
  begin
    FreeMem(FBuffers[i]);
  end;

  SetLength(FBuffers, 0);
  SetLength(FPosition, 0);
  SetLength(FSize, 0);
  SetLength(FIsFree, 0);

  inherited;
end;

function TBufferUtils.InsertBuffer(Data: PByte; Position, Size: Int64): Boolean;
var
  Item: Integer;
begin
  Result := False;
  Item := WaitFreeBuffer;

  if Item >= 0 then
  begin
    Move(Data^, FBuffers[Item]^, Size);
    FPosition[Item] := Position;
    FSize[Item] := Size;
    FIsFree[Item] := False;

    Result := True;
  end;
end;

function TBufferUtils.WaitFreeBuffer: Integer;
var
  IsExit, IsFirst: Boolean;
  i, c: Integer;
begin
  IsFirst := True;
  Result := -1;
  c := 0;

  repeat
    if IsFirst then
    begin
      IsFirst := False;
    end
    else
    begin
      Sleep(10);
    end;

    IsExit := False;

    for i := Low(FIsFree) to High(FIsFree) do
    begin
      IsExit := IsExit or FIsFree[i];
      if FIsFree[i] then
      begin
        Result := i;
        Break;
      end;
    end;

    Inc(c);
  until IsExit or (c > 20);
end;

{ *  Записывает все заполненные буферы в поток
  * }
function TBufferUtils.WriteStream(Stream: TStream): Int64;
var
  i: Integer;
  WriteSize: Integer;
begin
  Result := 0;

  for i := Low(FIsFree) to High(FIsFree) do
  begin
    if not FIsFree[i] then
    begin
      Stream.Position := FPosition[i];
      WriteSize := Stream.Write(FBuffers[i]^, FSize[i]);
      FIsFree[i] := True;

      Result := Result + WriteSize;
    end;
  end;
end;

end.
