﻿{ *  Управляет размерами и количеством частей в файле
  * }
unit PartManager;

interface

uses
  SysUtils, StreamUtils, IOUtils, Classes, PartInfo, Generics.Collections;

type
  TErrorMessage = (emForceCreateDirectories, emFileName, emOpenFile,
    emCreateFile, emReadFile, emChunkFailed, emFileVersion, emNoLink);

  { *
    Заголовок: string;
    Версия файла: Word;
    Ссылка: string;
    Имя файла: string;
    Размер файла: Int64;
    Время создания: TDateTime;
    Время последнего изменения: string;
    Количество частей: Cardinal;
    Количество скачанных частей: Cardinal;
    стартовая_позиция кол-во байт для скачивания
    ...
    * }

  TPartManager = class
  private
    FFilePath: string;
    FFileLink: string;
    FFileName: string;
    FFileSize: Int64;
    FBytesDownloaded: Int64;
    FPartsDownloaded: Cardinal;
    FIsErrorMessage: Boolean;
    FErrorMessage: TErrorMessage;
    FErrorDescription: string;
    FIsDirectoryCreate: Boolean;
    FCreateDate: TDateTime;
    FLastUpdate: string;
    FStream: TStreamUtils;
    FPartList: TList<TPartInfo>;
    FFileStream: TFileStream;
    FFileDeleted: Boolean;
    FOnFileChanged: TNotifyEvent;
    procedure EventFileChanged(Sender: TObject);
    procedure SetErrorMessage(ErrorMess: TErrorMessage); overload;
    procedure SetErrorMessage(ErrorMess: TErrorMessage;
      ErrorText: string); overload;
    function CreateFile(FilePath: string): Boolean;
    function WriteFile: Boolean;
  public
    constructor Create(FileName, DownloadPath, FileLink: string;
      FileSize: Int64; LastUpdate: string);
    destructor Destroy; override;

    function SetName(FileName: string; var InfoFileExists: Boolean): Boolean;
    procedure WriteInfo;
    procedure DeleteInfoFile;
    function OpenFile(FilePath: string): Boolean;

    property CreateDate: TDateTime read FCreateDate;
    property LastUpdate: string read FLastUpdate;
    property FileLink: string read FFileLink;
    property FileName: string read FFileName;
    property FilePath: string read FFilePath;
    property FileSize: Int64 read FFileSize;
    property BytesDownloaded: Int64 read FBytesDownloaded;
    property PartsDownloaded: Cardinal read FPartsDownloaded
      write FPartsDownloaded;
    property PartList: TList<TPartInfo> read FPartList write FPartList;

    property IsDirectoryCreate: Boolean read FIsDirectoryCreate;
    property IsErrorMessage: Boolean read FIsErrorMessage;
    property ErrorMessage: TErrorMessage read FErrorMessage;
    property ErrorDescription: string read FErrorDescription;

    property OnFileChanged: TNotifyEvent read FOnFileChanged
      write FOnFileChanged;
  end;

implementation

const
  FILE_EXTENTION: string = '.download';
  FILE_CHUNK: Cardinal = $BA52D728;
  FILE_VERSION: Word = 1;

  { TPartManager }

constructor TPartManager.Create(FileName, DownloadPath, FileLink: string;
  FileSize: Int64; LastUpdate: string);
begin
  FIsErrorMessage := False;
  FIsDirectoryCreate := False;
  FFileLink := FileLink;
  FFileSize := FileSize;
  FFileName := FileName;
  FLastUpdate := LastUpdate;

  FFilePath := DownloadPath;
  if not DirectoryExists(FFilePath) and not ForceDirectories(DownloadPath) then
  begin
    SetErrorMessage(emForceCreateDirectories);
  end
  else
    FIsDirectoryCreate := True;

  FPartList := TList<TPartInfo>.Create;
end;

procedure TPartManager.DeleteInfoFile;
var
  FileName: string;
begin
  FFileStream.Free;
  FPartList.Free;

  FileName := FFilePath + FFileName + FILE_EXTENTION;

  if FileExists(FileName) then
    FFileDeleted := DeleteFile(FileName);
end;

destructor TPartManager.Destroy;
begin
  if not FFileDeleted then
  begin
    FFileStream.Free;
    FPartList.Free;
  end;

  inherited;
end;

procedure TPartManager.EventFileChanged(Sender: TObject);
begin
  if Assigned(FOnFileChanged) then
    FOnFileChanged(Self);
end;

{ *  Устанавливает ошибку без описания
  * }
procedure TPartManager.SetErrorMessage(ErrorMess: TErrorMessage);
begin
  SetErrorMessage(ErrorMess, '');
end;

{ *  Устанавливает ошибку с описания
  * }
procedure TPartManager.SetErrorMessage(ErrorMess: TErrorMessage;
  ErrorText: string);
begin
  FIsErrorMessage := True;
  FErrorMessage := ErrorMess;
  FErrorDescription := ErrorText;
end;

{ *  Создает новый или открывает старый файл, содержащий информацию о
  *  загружаемом файле.
  * }
function TPartManager.SetName(FileName: string;
  var InfoFileExists: Boolean): Boolean;
begin
  InfoFileExists := False;
  Result := False;

  if not FIsDirectoryCreate then
  begin
    SetErrorMessage(emForceCreateDirectories);
  end
  else
  begin
    if TPath.HasValidFileNameChars(FileName + FILE_EXTENTION, False) then
    begin
      FFileName := FileName;

      if TFile.Exists(FFilePath + FileName + FILE_EXTENTION) then
      begin
        Result := OpenFile(FFilePath + FileName + FILE_EXTENTION);
        InfoFileExists := True;
      end
      else
      begin
        Result := CreateFile(FFilePath + FileName + FILE_EXTENTION);
        InfoFileExists := False;
      end;
    end
    else
    begin
      SetErrorMessage(emFileName, FileName);
    end;
  end;
end;

{ * Запись информации в файл
  * }
procedure TPartManager.WriteInfo;
begin
  WriteFile;
end;

function TPartManager.WriteFile: Boolean;
var
  I: Integer;
begin
  Result := False;
  FStream.FirstByte;

  // Запись заголовка
  if not FStream.WriteCardinal(FILE_CHUNK) then
    Exit;

  // Запись версии
  if not FStream.WriteWord(FILE_VERSION) then
    Exit;

  // Запись ссылки
  if not FStream.WriteString(FFileLink) then
    Exit;

  // Запись имени файла
  if not FStream.WriteString(FFileName) then
    Exit;

  // Запись размера файла
  if not FStream.WriteInt64(FFileSize) then
    Exit;

  // Запись времени создания файла
  if not FStream.WriteDouble(FCreateDate) then
    Exit;

  // Запись времени последнего изменения
  if not FStream.WriteString(FLastUpdate) then
    Exit;

  // Запись количества частей
  if not FStream.WriteCardinal(FPartList.Count) then
    Exit;

  // Запись количеств скачанных частей
  if not FStream.WriteCardinal(FPartsDownloaded) then
    Exit;

  // Запись стартовых позиций и количества байт для скачивания
  for I := 0 to FPartList.Count - 1 do
  begin
    if (not FStream.WriteInt64(FPartList[I].StartPosition)) or
      (not FStream.WriteInt64(FPartList[I].Count)) or
      not FStream.WriteInt64(FPartList[I].BytesDownloaded) then
      Exit;
  end;

  Result := True;
end;

{ *  Открывает файл и загружает из него всю информацию
  * }
function TPartManager.OpenFile(FilePath: string): Boolean;
var
  IsError: Boolean;
  FileChunk: Cardinal;
  DoubleValue: Double;

  FileVersion: Word;
  FileLink: WideString;
  DownloadFileName: WideString;
  CreateTime: TDateTime;
  LastUpdateTime: WideString;
  PartsCount: Cardinal;
  StartPosition: Int64;
  Count: Int64;
  BytesDownloaded: Int64;
  Bytes: Int64;
  FileSize: Int64;
  I: Integer;
  PartInfo: TPartInfo;
begin
  Result := False;

  try
    IsError := False;
    FFileStream := TFileStream.Create(FilePath, fmOpenReadWrite);
    FStream := TStreamUtils.Create(FFileStream);
  except
    on E: Exception do
    begin
      IsError := True;
      SetErrorMessage(emOpenFile, E.Message);
    end;
  end;

  if IsError then
    Exit;

  FStream.FirstByte;

  // Чтение заголовка файла
  if not FStream.ReadCardinal(FileChunk) then
  begin
    SetErrorMessage(emReadFile, 'OpenFile[1]');
    Exit;
  end;

  if FILE_CHUNK <> FileChunk then
  begin
    SetErrorMessage(emChunkFailed);
    Exit;
  end;

  // Чтение версии файла
  if not FStream.ReadWord(FileVersion) then
  begin
    SetErrorMessage(emReadFile, 'OpenFile[2]');
    Exit;
  end;

  if FileVersion > FILE_VERSION then
  begin
    SetErrorMessage(emFileVersion);
    Exit;
  end;

  // Чтение ссылки на файл
  if not FStream.ReadString(FileLink) then
  begin
    SetErrorMessage(emReadFile, 'OpenFile[3]');
    Exit;
  end;

  if Trim(FileLink) = '' then
  begin
    SetErrorMessage(emNoLink);
    Exit;
  end;

  // Чтение имени загружаемого файла
  if not FStream.ReadString(DownloadFileName) then
  begin
    SetErrorMessage(emReadFile, 'OpenFile[4]');
    Exit;
  end;

  // Чтение размера файла
  if not FStream.ReadInt64(FileSize) then
  begin
    SetErrorMessage(emReadFile, 'OpenFile[5]');
    Exit;
  end;

  if FileSize < 0 then
  begin
    SetErrorMessage(emReadFile, 'OpenFile[5]');
    Exit;
  end;

  // Чтение времени создания файла
  if not FStream.ReadDouble(DoubleValue) then
  begin
    SetErrorMessage(emReadFile, 'OpenFile[6]');
    Exit;
  end
  else
    Double(CreateTime) := DoubleValue;

  // Чтение времени последнего изменения файла
  if not FStream.ReadString(LastUpdateTime) then
  begin
    SetErrorMessage(emReadFile, 'OpenFile[7]');
    Exit;
  end;

  if LastUpdateTime <> FLastUpdate then
    EventFileChanged(Self);

  // Чтение количества частей
  if not FStream.ReadCardinal(PartsCount) then
  begin
    SetErrorMessage(emReadFile, 'OpenFile[8]');
    Exit;
  end;

  if not FStream.ReadCardinal(FPartsDownloaded) then
  begin
    SetErrorMessage(emReadFile, 'OpenFile[9]');
    Exit;
  end;

  // Чтение стартовой позиции и количество байт для скачивания у каждого потока
  Bytes := FileSize;
  for I := 0 to PartsCount - 1 do
  begin
    if (not FStream.ReadInt64(StartPosition)) or (not FStream.ReadInt64(Count))
      or not FStream.ReadInt64(BytesDownloaded) then
    begin
      SetErrorMessage(emReadFile, 'OpenFile[10]');
      Exit;
    end
    else
    begin
      Bytes := Bytes - Count;

      PartInfo.StartPosition := StartPosition + BytesDownloaded;
      PartInfo.Count := Count;
      PartInfo.BytesDownloaded := BytesDownloaded;
    end;

    FPartList.Add(PartInfo);
  end;

  FBytesDownloaded := Bytes;

  FFileName := FileName;
  FFileSize := FileSize;

  Result := True;
end;

{ *  Создает файл и генерирует информацию о загрузке
  * }
function TPartManager.CreateFile(FilePath: string): Boolean;
var
  IsError: Boolean;
begin
  Result := False;

  try
    IsError := False;
    FFileStream := TFileStream.Create(FilePath, fmCreate or fmOpenReadWrite);
    FStream := TStreamUtils.Create(FFileStream);
  except
    on E: Exception do
    begin
      IsError := True;
      SetErrorMessage(emCreateFile, E.Message);
    end;
  end;

  if IsError then
    Exit;

  FCreateDate := Now;

  FStream.Clear;

  Result := True;
end;

end.
