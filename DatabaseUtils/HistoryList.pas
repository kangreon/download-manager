unit HistoryList;

interface

uses
  Generics.Collections, States, DBUtils, DBTypes;

type
  THistrotyList = class(TList<THistoryItem>)
  private
    FDatabase: TDatabase;
  protected
    procedure Notify(const Item: THistoryItem;
      Action: TCollectionNotification); override;
  public
    constructor Create(Database: TDatabase);
  end;

implementation

{ THistrotyList }

constructor THistrotyList.Create(Database: TDatabase);
begin
  FDatabase := Database;
end;

procedure THistrotyList.Notify(const Item: THistoryItem;
  Action: TCollectionNotification);
begin
  inherited;

  case Action of
    cnAdded:
      begin
        FDatabase.AddToHistory(Item);
      end;
    cnRemoved:
      begin
        FDatabase.DeleteData(Item.DownloadID);
      end;
  end;
end;

end.
