unit DBUtils;

interface

uses
  Data.DB, Data.SqlExpr, Data.DbxSqlite, SysUtils, Classes, DBTypes,
  StringFileState;

const
  DriverName = 'Sqlite';
  DatabasePath = 'history.db';

type
  TDatabase = class
  private
    FSQLiteConnection: TSQLConnection;
    procedure ConnectDatabase;
    procedure CreateTables(Connection: TSQLConnection);
  public
    constructor Create;
    destructor Destroy; override;

    procedure AddData(Value: TDownloadItem);
    procedure UpdateData(Value: TDownloadItem);
    procedure DeleteData(N: Integer);
    procedure LoadData(out Value: TDownloadsTable);
    procedure GetRecord(N: Integer; out Value: TDownloadItem);

    procedure AddToHistory(Value: THistoryItem);
    procedure UpdateHistory(Value: THistoryItem);
    procedure DeleteHistory(N: Integer);
    procedure LoadDataHistory(out Value: THistoryTable);
    procedure GetHistoryRecord(N: Integer; out Value: THistoryItem);
  end;

implementation

{ TDatabase }

constructor TDatabase.Create;
begin
  ConnectDatabase;
end;

procedure TDatabase.CreateTables(Connection: TSQLConnection);
var
  Query: string;
begin
  Query := 'create table downloads(';
  Query := Query + 'download_id integer primary key autoincrement not null,';
  Query := Query + 'download_path varchar(255),';
  Query := Query + 'filename varchar(255),';
  Query := Query + 'filelink varchar(255),';
  Query := Query + 'partscount integer,';
  Query := Query + 'filestate varchar(255),';
  Query := Query + 'filesize integer)';

  Connection.Execute(Query, nil);

  Query := 'create table history(';
  Query := Query + 'download_id integer primary key autoincrement not null,';
  Query := Query + 'name varchar(255),';
  Query := Query + 'filelink varchar(255),';
  Query := Query + 'addtime varchar(255),';
  Query := Query + 'endtime varchar(255),';
  Query := Query + 'size integer,';
  Query := Query + 'path varchar(255))';

  Connection.Execute(Query, nil);
end;

procedure TDatabase.AddData(Value: TDownloadItem);
var
  Query: string;
begin
  Query := Format
    ('INSERT INTO downloads (download_path, filename, filelink, partscount, filestate, filesize) VALUES(''%s'', ''%s'', ''%s'', %d, ''%s'', %d)',
    [Value.DownloadPath, Value.FileName, Value.FileLink, Value.PartCount,
    FileStateToString(Value.FileState), Value.Size]);

  FSQLiteConnection.Execute(Query, nil);
end;

procedure TDatabase.AddToHistory(Value: THistoryItem);
var
  Query: string;
begin
  Query := Format
    ('INSERT INTO history (name, filelink, addtime, endtime, size, path) VALUES(''%s'', ''%s'', ''%s'', ''%s'', %d, ''%s'')',
    [Value.Name, Value.FileLink, DateToStr(Value.AddTime),
    DateToStr(Value.EndTime), Value.Size, Value.Path]);

  FSQLiteConnection.Execute(Query, nil);
end;

procedure TDatabase.ConnectDatabase;
var
  IsFileExists: Boolean;
begin
  IsFileExists := FileExists(DatabasePath);

  FSQLiteConnection := TSQLConnection.Create(nil);
  FSQLiteConnection.DriverName := DriverName;

  FSQLiteConnection.Params.Values['Database'] := DatabasePath;
  FSQLiteConnection.Params.Values['FailIfMissing'] := 'False';
  FSQLiteConnection.ExecuteDirect('PRAGMA auto_vacuum = 1');
  FSQLiteConnection.Connected := True;

  if not IsFileExists then
    CreateTables(FSQLiteConnection);
end;

procedure TDatabase.DeleteData(N: Integer);
var
  Query: string;
begin
  Query := Format('DELETE FROM downloads WHERE download_id = %d', [N + 1]);

  FSQLiteConnection.Execute(Query, nil);
end;

procedure TDatabase.DeleteHistory(N: Integer);
var
  Query: string;
begin
  Query := Format('DELETE FROM history WHERE download_id = %d', [N + 1]);

  FSQLiteConnection.Execute(Query, nil);
end;

destructor TDatabase.Destroy;
begin
  FSQLiteConnection.Free;

  inherited;
end;

procedure TDatabase.GetHistoryRecord(N: Integer; out Value: THistoryItem);
var
  Query: string;
var
  Data: TDataSet;
  Item: THistoryItem;
begin
  Query := Format('SELECT * FROM history WHERE download_id = %d', [N + 1]);
  FSQLiteConnection.Execute(Query, nil, Data);

  Data.Open;

  Item.DownloadID := Data.FieldByName('download_id').AsInteger;
  Item.Name := Data.FieldByName('name').AsString;
  Item.FileLink := Data.FieldByName('filelink').AsString;
  Item.AddTime := StrToDate(Data.FieldByName('addtime').AsString);
  Item.EndTime := StrToDate(Data.FieldByName('endtime').AsString);
  Item.Size := Data.FieldByName('size').AsInteger;
  Item.Path := Data.FieldByName('path').AsString;

  Value := Item;

  Data.Free;
end;

procedure TDatabase.GetRecord(N: Integer; out Value: TDownloadItem);
var
  Query: string;
var
  Data: TDataSet;
  Item: TDownloadItem;
begin
  Query := Format('SELECT * FROM downloads WHERE download_id = %d', [N + 1]);
  FSQLiteConnection.Execute(Query, nil, Data);

  Data.Open;

  Item.DownloadID := Data.FieldByName('download_id').AsInteger;
  Item.DownloadPath := Data.FieldByName('download_path').AsString;
  Item.FileName := Data.FieldByName('filename').AsString;
  Item.FileLink := Data.FieldByName('filelink').AsString;
  Item.PartCount := Data.FieldByName('partscount').AsInteger;
  Item.FileState := StringToFileState(Data.FieldByName('filestate').AsString);
  Item.Size := Data.FieldByName('filesize').AsInteger;

  Value := Item;

  Data.Free;
end;

procedure TDatabase.LoadData(out Value: TDownloadsTable);
var
  Query: string;
  Data: TDataSet;
  Item: TDownloadItem;
begin
  Query := 'SELECT * FROM downloads';
  FSQLiteConnection.Execute(Query, nil, Data);

  Data.Open;
  while not Data.Eof do
  begin
    Item.DownloadID := Data.FieldByName('download_id').AsInteger;
    Item.DownloadPath := Data.FieldByName('download_path').AsString;
    Item.FileName := Data.FieldByName('filename').AsString;
    Item.FileLink := Data.FieldByName('filelink').AsString;
    Item.PartCount := Data.FieldByName('partscount').AsInteger;
    Item.FileState := StringToFileState(Data.FieldByName('filestate').AsString);
    Item.Size := Data.FieldByName('filesize').AsInteger;

    Value.Add(Item);

    Data.Next;
  end;

  Data.Free;
end;

procedure TDatabase.LoadDataHistory(out Value: THistoryTable);
var
  Query: string;
  Data: TDataSet;
  Item: THistoryItem;
begin
  Query := 'SELECT * FROM history';
  FSQLiteConnection.Execute(Query, nil, Data);

  Data.Open;
  while not Data.Eof do
  begin
    Item.DownloadID := Data.FieldByName('download_id').AsInteger;
    Item.Name := Data.FieldByName('name').AsString;
    Item.FileLink := Data.FieldByName('filelink').AsString;
    Item.AddTime := StrToDate(Data.FieldByName('addtime').AsString);
    Item.EndTime := StrToDate(Data.FieldByName('endtime').AsString);
    Item.Size := Data.FieldByName('size').AsInteger;
    Item.Path := Data.FieldByName('path').AsString;

    Value.Add(Item);

    Data.Next;
  end;

  Data.Free;
end;

procedure TDatabase.UpdateData(Value: TDownloadItem);
var
  Query: string;
begin
  Query := Format
    ('UPDATE downloads SET download_path = ''%s'', filename = ''%s'', filelink = ''%s'', partscount = %d, filestate = ''%s'', filesize = %d WHERE download_id = %d',
    [Value.DownloadPath, Value.FileName, Value.FileLink, Value.PartCount,
    FileStateToString(Value.FileState), Value.Size, Value.DownloadID + 1]);

  FSQLiteConnection.Execute(Query, nil);
end;

procedure TDatabase.UpdateHistory(Value: THistoryItem);
var
  Query: string;
begin
  Query := Format
    ('UPDATE downloads SET name = ''%s'', filelink = ''%s'', addtime = ''%s'', endtime = ''%s'', size = %d, path = ''%s'' WHERE download_id = %d',
    [Value.Name, Value.FileLink, DateToStr(Value.AddTime),
    DateToStr(Value.EndTime), Value.Size, Value.Path, Value.DownloadID + 1]);

  FSQLiteConnection.Execute(Query, nil);
end;

end.
