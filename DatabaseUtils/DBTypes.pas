unit DBTypes;

interface

uses
  States, Generics.Collections;

type
  THistoryItem = record
    DownloadID: Integer;
    Name: string;
    FileLink: string;
    AddTime: TDateTime;
    EndTime: TDateTime;
    Size: Int64;
    Path: string;
  end;

  TDownloadItem = record
    DownloadID: Integer;
    DownloadPath: string;
    FileName: string;
    FileLink: string;
    FileState: TFileState;
    PartCount: Integer;
    Size: Int64;
  end;

  TDownloadsTable = TList<TDownloadItem>;
  THistoryTable = TList<THistoryItem>;

implementation

end.
