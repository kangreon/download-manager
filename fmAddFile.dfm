object AddFileForm: TAddFileForm
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #1044#1086#1073#1072#1074#1083#1077#1085#1080#1077' '#1092#1072#1081#1083#1072
  ClientHeight = 476
  ClientWidth = 388
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    AlignWithMargins = True
    Left = 3
    Top = 3
    Width = 382
    Height = 435
    Align = alClient
    Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1080' '#1079#1072#1075#1088#1091#1079#1082#1080': '
    TabOrder = 0
    object Label1: TLabel
      Left = 19
      Top = 24
      Width = 89
      Height = 13
      Caption = #1057#1089#1099#1083#1082#1072' '#1085#1072' '#1092#1072#1081#1083': '
    end
    object Label2: TLabel
      Left = 19
      Top = 70
      Width = 71
      Height = 13
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1074': '
    end
    object Label3: TLabel
      Left = 19
      Top = 169
      Width = 97
      Height = 13
      Caption = #1057#1089#1099#1083#1082#1080' '#1085#1072' '#1092#1072#1081#1083#1099': '
      Enabled = False
    end
    object Label4: TLabel
      Left = 19
      Top = 380
      Width = 89
      Height = 13
      Caption = #1044#1077#1083#1080#1090#1100' '#1092#1072#1081#1083' '#1085#1072': '
    end
    object Button1: TButton
      Left = 303
      Top = 89
      Width = 66
      Height = 21
      Caption = #1054#1073#1079#1086#1088
      TabOrder = 0
      OnClick = Button1Click
    end
    object CheckBox1: TCheckBox
      Left = 19
      Top = 146
      Width = 156
      Height = 17
      Caption = #1052#1085#1086#1078#1077#1089#1090#1074#1077#1085#1085#1072#1103' '#1079#1072#1075#1088#1091#1079#1082#1072' '
      TabOrder = 1
      OnClick = CheckBox1Click
    end
    object CheckBox2: TCheckBox
      Left = 19
      Top = 123
      Width = 265
      Height = 17
      Caption = #1048#1089#1087#1086#1083#1100#1079#1086#1074#1072#1090#1100' '#1087#1091#1090#1080' '#1086#1087#1077#1088#1076#1077#1083#1105#1085#1085#1099#1077' '#1074' '#1085#1072#1089#1090#1088#1086#1081#1082#1072#1093
      TabOrder = 2
      OnClick = CheckBox2Click
    end
    object Edit1: TEdit
      Left = 19
      Top = 43
      Width = 350
      Height = 21
      TabOrder = 3
      OnChange = Edit1Change
    end
    object Edit2: TEdit
      Left = 19
      Top = 89
      Width = 278
      Height = 21
      ReadOnly = True
      TabOrder = 4
      OnChange = Edit1Change
    end
    object Memo1: TMemo
      Left = 19
      Top = 188
      Width = 350
      Height = 178
      Enabled = False
      TabOrder = 5
    end
    object ComboBox1: TComboBox
      Left = 19
      Top = 399
      Width = 156
      Height = 21
      Style = csDropDownList
      ItemIndex = 3
      TabOrder = 6
      Text = '4 '#1095#1072#1089#1090#1080
      Items.Strings = (
        '1 '#1095#1072#1089#1090#1100
        '2 '#1095#1072#1089#1090#1080
        '3 '#1095#1072#1089#1090#1080
        '4 '#1095#1072#1089#1090#1080
        '5 '#1095#1072#1089#1090#1077#1081
        '6 '#1095#1072#1089#1090#1077#1081
        '7 '#1095#1072#1089#1090#1077#1081
        '8 '#1095#1072#1089#1090#1077#1081
        '9 '#1095#1072#1089#1090#1077#1081
        '10 '#1095#1072#1089#1090#1077#1081)
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 441
    Width = 388
    Height = 35
    Align = alBottom
    TabOrder = 1
    object Button2: TButton
      Left = 103
      Top = 3
      Width = 75
      Height = 25
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      Enabled = False
      TabOrder = 0
      OnClick = Button2Click
    end
    object Button3: TButton
      Left = 22
      Top = 3
      Width = 75
      Height = 25
      Caption = #1054#1090#1084#1077#1085#1072
      TabOrder = 1
      OnClick = Button3Click
    end
  end
end
