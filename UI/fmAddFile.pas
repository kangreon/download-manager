unit fmAddFile;

interface

uses
  Forms, StdCtrls, Controls, Classes, Vcl.ExtCtrls, Windows, Vcl.FileCtrl;

type
  TAddFileForm = class(TForm)
    eFileLink: TEdit;
    eFilePath: TEdit;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Label1: TLabel;
    Label2: TLabel;
    cbMultipleDownload: TCheckBox;
    mDownloadsLinks: TMemo;
    cbSettingsPath: TCheckBox;
    Label3: TLabel;
    GroupBox1: TGroupBox;
    Panel1: TPanel;
    Label4: TLabel;
    cbPartsCount: TComboBox;
    procedure Button3Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cbSettingsPathClick(Sender: TObject);
    procedure cbMultipleDownloadClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    FPartsCount: Integer;
    FFileLink: string;
    FFilesLinks: TStrings;
    FDownloadPath: string;
    FDownloadsPath: TStrings;
    FMultipleDownload: Boolean;
    FSettingsPath: Boolean;
    FOnGetData: TNotifyEvent;
    procedure EventGetData;
  public
    property PartsCount: Integer read FPartsCount;
    property FileLink: string read FFileLink;
    property FilesLinks: TStrings read FFilesLinks;
    property DownloadPath: string read FDownloadPath;
    property DownloadsPath: TStrings read FDownloadsPath;
    property MultipleDownload: Boolean read FMultipleDownload;
    property SettingsPath: Boolean read FSettingsPath;

    property OnGetData: TNotifyEvent read FOnGetData write FOnGetData;
  end;

var
  AddFileForm: TAddFileForm;

implementation

{$R *.dfm}

procedure TAddFileForm.Button1Click(Sender: TObject);
var
  sDir: String;
begin
  if SelectDirectory('�������� �������', '', sDir,
    [sdNewFolder, sdNewUI, sdValidateDir], Self) then
  begin
    if sDir[Length(sDir)] <> '\' then
      sDir := sDir + '\';

    eFilePath.Text := sDir;
  end;
end;

procedure TAddFileForm.Button2Click(Sender: TObject);
var
  ErrorMsg: Boolean;
begin
  ErrorMsg := False;

  if (cbMultipleDownload.Checked) and (mDownloadsLinks.Lines.Count = 0) or
    (eFilePath.Text = '') then
    ErrorMsg := True;

  if (not cbMultipleDownload.Checked) and
    ((eFileLink.Text = '') or (eFilePath.Text = '')) then
    ErrorMsg := True;

  if (cbSettingsPath.Checked) and ((mDownloadsLinks.Lines.Count = 0) or
    (eFileLink.Text = '')) then
    ErrorMsg := True;

  if ErrorMsg then
  begin
    MessageBox(handle, PChar('�� �� ��������� ������ ����!'), PChar('������'),
      MB_OK or MB_ICONWARNING);
    Exit;
  end;

  FFileLink := eFileLink.Text;
  FDownloadPath := eFilePath.Text;
  FSettingsPath := cbSettingsPath.Checked;
  FMultipleDownload := cbMultipleDownload.Checked;
  FFilesLinks := mDownloadsLinks.Lines;
  FPartsCount := cbPartsCount.ItemIndex + 1;

  EventGetData;

  Close;
end;

procedure TAddFileForm.Button3Click(Sender: TObject);
begin
  Close;
end;

procedure TAddFileForm.cbMultipleDownloadClick(Sender: TObject);
begin
  if cbMultipleDownload.Checked then
  begin
    Label3.Enabled := True;
    mDownloadsLinks.Enabled := True;
    Label1.Enabled := False;
    eFileLink.Enabled := False;
  end
  else
  begin
    Label3.Enabled := False;
    mDownloadsLinks.Enabled := False;
    Label1.Enabled := True;
    eFileLink.Enabled := True;
  end;
end;

procedure TAddFileForm.cbSettingsPathClick(Sender: TObject);
begin
  if cbSettingsPath.Checked then
  begin
    Label2.Enabled := False;
    eFilePath.Enabled := False;
    Button1.Enabled := False;
  end
  else
  begin
    Label2.Enabled := True;
    eFilePath.Enabled := True;
    Button1.Enabled := True;
  end;
end;

procedure TAddFileForm.EventGetData;
begin
  if Assigned(FOnGetData) then
    FOnGetData(Self);
end;

procedure TAddFileForm.FormCreate(Sender: TObject);
begin
  Position := poScreenCenter;
end;

end.
