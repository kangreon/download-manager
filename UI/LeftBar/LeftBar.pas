unit LeftBar;

interface

uses
  UITypes, Graphics, Controls, Classes, Windows, ComCtrls, Forms, PNGImage, StrUtils;

type
  TProcSelectItem = procedure(Sender: TObject; N: Integer) of object;

  TLeftBar = class
  private
    FOnSelectItem: TProcSelectItem;
    procedure EventSelectItem(Sender: TObject; N: Integer);
    procedure CustomDrawItem(Sender: TCustomTreeView; Node: TTreeNode;
      State: TCustomDrawState; var DefaultDraw: Boolean);
    procedure TreeViewChange(Sender: TObject; Node: TTreeNode);
  public
    constructor Create(TreeView: TTreeView);
    destructor Destroy; override;

    property OnSelectItem: TProcSelectItem read FOnSelectItem
      write FOnSelectItem;
  end;

implementation

{ TLeftBar }

constructor TLeftBar.Create(TreeView: TTreeView);
begin
  TreeView.Align := alLeft;
  TreeView.Width := 145;
  TreeView.BorderStyle := bsNone;
  TreeView.Color := RGB(42, 43, 44);
  TreeView.HotTrack := True;
  TreeView.ReadOnly := True;
  TreeView.Indent := 10;
  TreeView.Font.Name := 'Tahoma';
  TreeView.Font.Size := 8;
  TreeView.HideSelection := False;
  TreeView.OnCustomDrawItem := CustomDrawItem;
  TreeView.OnChange := TreeViewChange;

  TreeView.Items.Add(nil, '��������');
  TreeView.Items.AddChild(TreeView.Items.Item[0], '��������');
  TreeView.Items.AddChild(TreeView.Items.Item[0], '����������');
  TreeView.Items.AddChild(TreeView.Items.Item[0], '�����������');
  TreeView.Items.AddChild(TreeView.Items.Item[0], '������');

  TreeView.Items.Add(TreeView.Items.Item[0], '�������');
  TreeView.Items.AddChild(TreeView.Items.Item[5], '����');
  TreeView.Items.AddChild(TreeView.Items.Item[5], '������');
  TreeView.Items.AddChild(TreeView.Items.Item[5], '�����');
  TreeView.Items.AddChild(TreeView.Items.Item[5], '���');

  TreeView.Items.Item[0].Selected := True;
end;

procedure TLeftBar.CustomDrawItem(Sender: TCustomTreeView; Node: TTreeNode;
  State: TCustomDrawState; var DefaultDraw: Boolean);
var
  NodeRect: TRect;
  Icon: TPNGImage;
  Icon2: TPNGImage;
begin
  DefaultDraw := False;

  Sender.Canvas.Font.Color := RGB(211, 211, 211);
  Sender.Canvas.Font.Size := 10;
  Sender.Canvas.Brush.Style := bsClear;

  NodeRect := Node.DisplayRect(True);
  if (cdsFocused in State) or (cdsSelected in State) or (cdsHot in State) then
    Sender.Canvas.Font.Style := Sender.Canvas.Font.Style + [fsBold];

  if (cdsSelected in State) and (Node.Level = 1) then
    NodeRect.Left := NodeRect.Left + 5;

  NodeRect.Left := NodeRect.Left + 10;

  Sender.Canvas.TextOut(NodeRect.Left, NodeRect.Top, Node.Text);

  if Node.Level = 0 then
  begin
    Icon := TPNGImage.Create;
    if Node.Expanded then
    begin
      Icon.LoadFromResourceName(0, 'arrow');
      NodeRect.Left := NodeRect.Left - 15;
      NodeRect.Top := NodeRect.Top + 5;
    end
    else
    begin
      Icon.LoadFromResourceName(0, 'arrow2');;
      NodeRect.Left := NodeRect.Left - 12;
      NodeRect.Top := NodeRect.Top + 4;
    end;

    Sender.Canvas.Draw(NodeRect.Left, NodeRect.Top, Icon);
    Icon.Free;
  end;

  Icon2 := TPNGImage.Create;

  if Node.Text = '�������' then
  begin
    NodeRect.Left := NodeRect.Left - 5;
  end;

  if Node.Level = 1 then
  begin
    NodeRect.Left := NodeRect.Left - 15;
    NodeRect.Top := NodeRect.Top + 5;

    case AnsiIndexStr(Node.Text, ['��������', '����������', '�����������',
      '������', '����', '������', '�����', '���']) of
      0:
        begin
          Icon2.LoadFromResourceName(0, 'active');
          NodeRect.Left := NodeRect.Left + 2;
        end;
      1:
        begin
          Icon2.LoadFromResourceName(0, 'noactive');
          NodeRect.Left := NodeRect.Left + 2;
        end;
      2:
        begin
          Icon2.LoadFromResourceName(0, 'cancel');
          NodeRect.Left := NodeRect.Left + 2;
        end;
      3:
        begin
          Icon2.LoadFromResourceName(0, 'error');
          NodeRect.Left := NodeRect.Left + 2;
        end;
      4:
        begin
          Icon2.LoadFromResourceName(0, 'day');
          NodeRect.Left := NodeRect.Left - 6;
          NodeRect.Top := NodeRect.Top - 1;
        end;
      5:
        begin
          Icon2.LoadFromResourceName(0, 'week');
          NodeRect.Left := NodeRect.Left - 6;
          NodeRect.Top := NodeRect.Top - 1;
        end;
      6:
        begin
          Icon2.LoadFromResourceName(0, 'month');
          NodeRect.Left := NodeRect.Left - 6;
          NodeRect.Top := NodeRect.Top - 1;
        end;
      7:
        begin
          Icon2.LoadFromResourceName(0, 'year');;
          NodeRect.Left := NodeRect.Left - 6;
          NodeRect.Top := NodeRect.Top - 1;
        end;
    end;
    Sender.Canvas.Draw(NodeRect.Left, NodeRect.Top, Icon2);
  end;

  Icon2.Free;
end;

destructor TLeftBar.Destroy;
begin

  inherited;
end;

procedure TLeftBar.EventSelectItem(Sender: TObject; N: Integer);
begin
  if Assigned(FOnSelectItem) then
    FOnSelectItem(Sender, N);
end;

procedure TLeftBar.TreeViewChange(Sender: TObject; Node: TTreeNode);
var
  N: Integer;
begin
  N := 0;

  if (Node.Level = 0) and (Node.Text = '��������') then
    N := 0;

  if Node.Level = 1 then
  begin
    case AnsiIndexStr(Node.Text, ['��������', '����������', '�����������',
      '������']) of
      0:
        N := 1;
      1:
        N := 2;
      2:
        N := 3;
      3:
        N := 4;
    end;
  end;

  EventSelectItem(Sender, N);
end;

end.
