unit FileAdd;

interface

uses
  Classes, fmAddFile;

type
  TFileAdd = class
  private
    FPartsCount: Integer;
    FFileLink: string;
    FFilesLinks: TStrings;
    FDownloadPath: string;
    FDownloadsPath: TStrings;
    FMultipleDownload: Boolean;
    FSettingsPath: Boolean;
    FOnGetInfo: TNotifyEvent;
    procedure EventGetInfo;
    function GetPartsCount: Integer;
    function GetFileLink: string;
    function GetFilesLinks: TStrings;
    function GetDownloadPath: string;
    function GetDownloadsPath: TStrings;
    function GetMultipleDownload: Boolean;
    function GetSettingsPath: Boolean;
    procedure GetInfo(Sender: TObject);
  public
    constructor Create;
    destructor Destroy; override;

    property PartsCount: Integer read GetPartsCount;
    property FileLink: string read GetFileLink;
    property FilesLinks: TStrings read GetFilesLinks;
    property DownloadPath: string read GetDownloadPath;
    property DownloadsPath: TStrings read GetDownloadsPath;
    property MultipleDownload: Boolean read GetMultipleDownload;
    property SettingsPath: Boolean read GetSettingsPath;

    property OnGetInfo: TNotifyEvent read FOnGetInfo write FOnGetInfo;
  end;

implementation

{ TFileAdd }

constructor TFileAdd.Create;
begin
  AddFileForm := TAddFileForm.Create(nil);
  AddFileForm.OnGetData := GetInfo;
end;

destructor TFileAdd.Destroy;
begin

  inherited;
end;

procedure TFileAdd.EventGetInfo;
begin
  if Assigned(FOnGetInfo) then
    FOnGetInfo(Self);
end;

function TFileAdd.GetDownloadPath: string;
begin
  Result := FDownloadPath;
end;

function TFileAdd.GetDownloadsPath: TStrings;
begin
  Result := FDownloadsPath;
end;

function TFileAdd.GetFileLink: string;
begin
  Result := FFileLink;
end;

function TFileAdd.GetFilesLinks: TStrings;
begin
  Result := FFilesLinks;
end;

procedure TFileAdd.GetInfo(Sender: TObject);
begin
  FFileLink := AddFileForm.FileLink;
  FFilesLinks := AddFileForm.FilesLinks;
  FDownloadPath := AddFileForm.DownloadPath;
  FDownloadsPath := AddFileForm.DownloadsPath;
  FMultipleDownload := AddFileForm.MultipleDownload;
  FSettingsPath := AddFileForm.SettingsPath;
  FPartsCount := AddFileForm.PartsCount;

  EventGetInfo;
end;

function TFileAdd.GetMultipleDownload: Boolean;
begin
  Result := FMultipleDownload;
end;

function TFileAdd.GetPartsCount: Integer;
begin
  Result := FPartsCount;
end;

function TFileAdd.GetSettingsPath: Boolean;
begin
  Result := FSettingsPath;
end;

end.
