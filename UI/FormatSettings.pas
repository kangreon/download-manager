unit FormatSettings;

interface

uses
  SysUtils;

function FormatByteSize(const bytes: Longint): string;

implementation

function FormatByteSize(const bytes: Longint): string;
const
  B = 1; // byte
  KB = 1024 * B; // kilobyte
  MB = 1024 * KB; // megabyte
  GB = 1024 * MB; // gigabyte
begin
  if bytes = 0 then
    Result := '0 �'
  else if bytes > GB then
    Result := FormatFloat('#.## ���', bytes / GB)
  else if bytes > MB then
    Result := FormatFloat('#.## ���', bytes / MB)
  else if bytes > KB then
    Result := FormatFloat('#.## ���', bytes / KB)
  else
    Result := FormatFloat('#.## �', bytes);
end;

end.
