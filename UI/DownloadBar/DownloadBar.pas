unit DownloadBar;

interface

uses
  Graphics, Windows, VirtualTrees, Forms, SysUtils, Classes, States, StrUtils;

type
  PTreeData = ^TTreeData;

  TTreeData = record
    FileName: string;
    FileSize: string;
    FileState: TFileState;
    DownloadSpeed: string;
    RemainTime: string;
    Percents: Real;
  end;

  TDownloadBar = class
  private
    procedure VSTFocusChanged(Sender: TBaseVirtualTree; Node: PVirtualNode;
      Column: TColumnIndex);
    procedure VSTFreeNode(Sender: TBaseVirtualTree; Node: PVirtualNode);
    procedure VSTGetNodeDataSize(Sender: TBaseVirtualTree;
      var NodeDataSize: Integer);
    procedure VSTGetText(Sender: TBaseVirtualTree; Node: PVirtualNode;
      Column: TColumnIndex; TextType: TVSTTextType; var CellText: string);
    procedure VSTNewText(Sender: TBaseVirtualTree; Node: PVirtualNode;
      Column: TColumnIndex; NewText: string);
    procedure VSTBeforeCellPaint(Sender: TBaseVirtualTree;
      TargetCanvas: TCanvas; Node: PVirtualNode; Column: TColumnIndex;
      CellPaintMode: TVTCellPaintMode; CellRect: TRect; var ContentRect: TRect);
    procedure VSTChange(Sender: TBaseVirtualTree; Node: PVirtualNode);
    function NodeByIndex(const aTree: TBaseVirtualTree; const anIndex: Integer)
      : PVirtualNode;
    function Find(const S, P: string): Integer;
  public
    constructor Create(VST: TVirtualStringTree;
      ClientWidth, ClientHeight: Integer);
    destructor Destroy; override;

    procedure Resize(VST: TVirtualStringTree;
      ClientWidth, ClientHeight: Integer);

    procedure AddDownload(VST: TBaseVirtualTree; FileName, FileSize: string;
      FileState: TFileState; DownloadSpeed, RemainTime: string);
    procedure AddInfo(VST: TBaseVirtualTree; N: Integer; ProgressPercent: Real;
      FileName, FileSize: string; FileState: TFileState;
      DownloadSpeed, RemainTime: string);
    procedure DeleteDownload(VST: TBaseVirtualTree; N: Integer);

    function FindDownload(VST: TBaseVirtualTree; P: string): Integer;
  end;

implementation

{ TLeftBar }

procedure TDownloadBar.AddDownload(VST: TBaseVirtualTree;
  FileName, FileSize: string; FileState: TFileState;
  DownloadSpeed, RemainTime: string);
var
  Data: PTreeData;
  Node: PVirtualNode;
begin
  Node := VST.AddChild(nil);

  Data := VST.GetNodeData(Node);
  Data^.FileName := FileName;
  Data^.FileSize := FileSize;
  Data^.FileState := FileState;
  Data^.DownloadSpeed := DownloadSpeed;
  Data^.RemainTime := RemainTime;
  Data^.Percents := 0;

  VST.InvalidateNode(Node);
end;

procedure TDownloadBar.AddInfo(VST: TBaseVirtualTree; N: Integer;
  ProgressPercent: Real; FileName, FileSize: string; FileState: TFileState;
  DownloadSpeed, RemainTime: string);
var
  Data: PTreeData;
  Node: PVirtualNode;
begin
  VST.BeginUpdate;

  Node := NodeByIndex(VST, N);

  if not Assigned(Node) then
    Exit;

  Data := VST.GetNodeData(Node);

  Data.FileName := FileName;
  Data.FileSize := FileSize;
  Data.FileState := FileState;
  Data.DownloadSpeed := DownloadSpeed;
  Data.RemainTime := RemainTime;
  Data.Percents := ProgressPercent;

  VST.InvalidateNode(Node);

  VST.EndUpdate;
end;

constructor TDownloadBar.Create(VST: TVirtualStringTree;
  ClientWidth, ClientHeight: Integer);
var
  I: Integer;
begin
  VST.OnBeforeCellPaint := VSTBeforeCellPaint;
  VST.OnFocusChanged := VSTFocusChanged;
  VST.OnFreeNode := VSTFreeNode;
  VST.OnGetNodeDataSize := VSTGetNodeDataSize;
  VST.OnGetText := VSTGetText;
  VST.OnNewText := VSTNewText;
  VST.OnChange := VSTChange;

  for I := 0 to 4 do
    VST.Header.Columns.Add;

  VST.Header.Columns.Items[0].Text := '��� �����';
  VST.Header.Columns.Items[0].Width := 150;
  VST.Header.Columns.Items[0].Alignment := taCenter;

  VST.Header.Columns.Items[1].Text := '������ �����';
  VST.Header.Columns.Items[1].Width := 90;
  VST.Header.Columns.Items[1].Alignment := taCenter;

  VST.Header.Columns.Items[2].Text := '��������';
  VST.Header.Columns.Items[2].Width := 140;
  VST.Header.Columns.Items[2].Alignment := taCenter;

  VST.Header.Columns.Items[3].Text := '�������� ��������';
  VST.Header.Columns.Items[3].Width := 110;
  VST.Header.Columns.Items[3].Alignment := taCenter;

  VST.Header.Columns.Items[4].Text := '���������� �����';
  VST.Header.Columns.Items[4].Width := 110;
  VST.Header.Columns.Items[4].Alignment := taCenter;

  VST.Header.Options := [hoVisible, hoColumnResize, hoDrag, hoShowSortGlyphs];
  VST.Header.Style := hsFlatButtons;
  VST.TreeOptions.AutoOptions := [toAutoDropExpand, toAutoScrollOnExpand,
    toAutoSort, toAutoTristateTracking, toAutoDeleteMovedNodes,
    toAutoChangeScale];
  VST.TreeOptions.MiscOptions := [toAcceptOLEDrop, toFullRepaintOnResize,
    toInitOnSave, toReportMode, toToggleOnDblClick, toWheelPanning];
  VST.TreeOptions.PaintOptions := [toHideFocusRect, toShowButtons,
    toShowDropmark, toShowHorzGridLines, toShowVertGridLines, toThemeAware,
    toUseBlendedImages, toFullVertGridLines, toFixedIndent, toUseExplorerTheme,
    toShowFilteredNodes, toUseBlendedSelection];
  VST.TreeOptions.SelectionOptions := [toFullRowSelect, toFullRowSelect,
    toMultiSelect];

  VST.BorderStyle := bsNone;
end;

procedure TDownloadBar.DeleteDownload(VST: TBaseVirtualTree; N: Integer);
begin
  VST.DeleteSelectedNodes;
end;

destructor TDownloadBar.Destroy;
begin

  inherited;
end;

procedure TDownloadBar.Resize(VST: TVirtualStringTree;
  ClientWidth, ClientHeight: Integer);
begin
  if Assigned(VST) then
  begin
    VST.Left := 145;
    VST.Top := 44;
    VST.Width := ClientWidth - 145;
    VST.Height := ClientHeight - 44 - 75;

    VST.Header.Columns.Items[2].Width := VST.Width - VST.Header.Columns.Items[3]
      .Width - VST.Header.Columns.Items[4].Width - VST.Header.Columns.Items[1]
      .Width - VST.Header.Columns.Items[0].Width;
  end;
end;

procedure TDownloadBar.VSTBeforeCellPaint(Sender: TBaseVirtualTree;
  TargetCanvas: TCanvas; Node: PVirtualNode; Column: TColumnIndex;
  CellPaintMode: TVTCellPaintMode; CellRect: TRect; var ContentRect: TRect);
var
  NodeData: PTreeData;
  ProgressBarRect: TRect;
  Color: TColor;
begin
  NodeData := Sender.GetNodeData(Node);

  // ��������-��� ��� �������� ���������� ������
  if Column = 2 then
  begin
    ProgressBarRect.Left := CellRect.Left;
    ProgressBarRect.Top := CellRect.Top;
    ProgressBarRect.Right := Round((CellRect.Right - CellRect.Left) *
      NodeData.Percents) + CellRect.Left;

    ProgressBarRect.Bottom := CellRect.Bottom;

    Color := clWhite;

    case NodeData.FileState of
      fsDownloading, fsGetInfo:
        Color := RGB(80, 198, 80);
      fsComplete:
        Color := RGB(80, 182, 80);
      fsStopped:
        Color := RGB(172, 172, 172);
      fsError:
        Color := RGB(245, 0, 39);
    end;

    if (ProgressBarRect.Right - ProgressBarRect.Left) > 0 then
    begin
      TargetCanvas.Brush.Color := Color;
      TargetCanvas.FillRect(ProgressBarRect);
    end;
  end;
end;

procedure TDownloadBar.VSTChange(Sender: TBaseVirtualTree; Node: PVirtualNode);
begin
  Sender.Refresh;
end;

procedure TDownloadBar.VSTFocusChanged(Sender: TBaseVirtualTree;
  Node: PVirtualNode; Column: TColumnIndex);
begin
  Sender.Refresh;
end;

procedure TDownloadBar.VSTFreeNode(Sender: TBaseVirtualTree;
  Node: PVirtualNode);
var
  Data: PTreeData;
begin
  Data := Sender.GetNodeData(Node);
  if Assigned(Data) then
  begin
    Data^.FileName := '';
    Data^.FileSize := '';
    Data^.FileState := fsStopped;
    Data^.Percents := 0;
    Data^.DownloadSpeed := '';
    Data^.RemainTime := '';
  end;
end;

procedure TDownloadBar.VSTGetNodeDataSize(Sender: TBaseVirtualTree;
  var NodeDataSize: Integer);
begin
  NodeDataSize := SizeOf(TTreeData);
end;

procedure TDownloadBar.VSTGetText(Sender: TBaseVirtualTree; Node: PVirtualNode;
  Column: TColumnIndex; TextType: TVSTTextType; var CellText: string);
var
  Data: PTreeData;
  Text: string;
begin
  Data := Sender.GetNodeData(Node);
  case Column of
    0:
      CellText := Data^.FileName;
    1:
      CellText := Data^.FileSize;
    2:
      begin
        case Data^.FileState of
          fsDownloading:
            Text := '��������';
          fsComplete:
            Text := '���������';
          fsStopped:
            Text := '�����������';
          fsError:
            Text := '������';
        end;

        CellText := Text;
      end;
    3:
      CellText := Data^.DownloadSpeed;
    4:
      CellText := Data^.RemainTime;
  end;
end;

procedure TDownloadBar.VSTNewText(Sender: TBaseVirtualTree; Node: PVirtualNode;
  Column: TColumnIndex; NewText: string);
var
  Data: PTreeData;
  FileSt: TFileState;
begin
  Data := Sender.GetNodeData(Node);
  case Column of
    0:
      Data^.FileName := NewText;
    1:
      Data^.FileSize := NewText;
    2:
      begin
        FileSt := fsError;

        case AnsiIndexStr(NewText, ['��������', '���������', '�����������',
          '������']) of
          0:
            FileSt := fsDownloading;
          1:
            FileSt := fsComplete;
          2:
            FileSt := fsStopped;
          3:
            FileSt := fsError;
        end;

        Data^.Percents := 0;
        Data^.FileState := FileSt;
      end;
    3:
      Data^.DownloadSpeed := NewText;
    4:
      Data^.RemainTime := NewText;
  end;
end;

function TDownloadBar.NodeByIndex(const aTree: TBaseVirtualTree;
  const anIndex: Integer): PVirtualNode;
var
  Cnt: Integer;
  Node: PVirtualNode;
begin
  Node := aTree.GetFirst(False);
  Cnt := 0;

  while (Node <> nil) and (Cnt < anIndex) do
  begin
    Node := aTree.GetNext(Node);
    inc(Cnt);
  end;

  Result := Node;
end;

function TDownloadBar.Find(const S, P: string): Integer;
var
  I, j: Integer;
begin
  Result := 0;
  if Length(P) > Length(S) then
    Exit;
  for I := 1 to Length(S) - Length(P) + 1 do
    for j := 1 to Length(P) do
      if P[j] <> S[I + j - 1] then
        Break
      else if j = Length(P) then
      begin
        Result := I;
        Exit;
      end;
end;

function TDownloadBar.FindDownload(VST: TBaseVirtualTree; P: string): Integer;
var
  Last: Integer;
  Node: PVirtualNode;
  I: Integer;
  Data: PTreeData;
begin
  Result := -1;

  Node := VST.GetLast;
  if Assigned(Node) then
    Last := Node.Index
  else
    Exit;

  for I := 0 to Last do
  begin
    Node := NodeByIndex(VST, I);
    Data := VST.GetNodeData(Node);

    if Find(Data.FileName, P) <> 0 then
    begin
      Result := I;
      Break;
    end
    else if I = Last then
      Result := -1;
  end;

  if Result <> -1 then
  begin
    for I := 0 to Last do
    begin
      Node := NodeByIndex(VST, I);
      VST.Selected[Node] := False;
    end;

    Node := NodeByIndex(VST, Result);
    VST.Selected[Node] := True;
  end
  else
  begin
    for I := 0 to Last do
    begin
      Node := NodeByIndex(VST, I);
      VST.Selected[Node] := False;
    end;
  end;

end;

end.
