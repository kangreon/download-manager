unit Downloads;

interface

uses
  FileAdd, FileDownload, ComCtrls, States, Classes, DBUtils,
  Generics.Collections, SysUtils, PartManager, DBTypes;

type
  TFiles = TList<TFileDownload>;

  TDownloads = class
  private
    FFiles: TFiles;
    FFilesBuf: TFiles;
    FHistory: THistoryTable;
    FOnEndDownload: TNotifyEvent;
    FOnStartDownload: TNotifyEvent;
    FFileAdd: TFileAdd;
    FOnGetData: TNotifyEvent;
    FDatabase: TDatabase;
    procedure DownloadStart(Sender: TObject);
    procedure DownloadEnd(Sender: TObject; FileState: TFileState;
      ThreadID: Integer);
    procedure EventEndDownload;
    procedure EventDownloadStart;
    procedure AddDownloads;
    procedure GetData(Sender: TObject);
    procedure EventGetData;
    procedure DownloadInit(Sender: TObject; ThreadID: Integer);
    procedure LoadHistory;
  public
    constructor Create;
    destructor Destroy; override;

    procedure DeleteDownload(N: Integer);
    procedure StopDownload(N: Integer);
    procedure AddDownload(N: Integer);
    procedure UpDownload(N: Integer);
    procedure DownDownload(N: Integer);
    procedure LoadDataFromDatabase;
    procedure DeleteRecord(N: Integer);

    property OnEndDownload: TNotifyEvent read FOnEndDownload
      write FOnEndDownload;
    property OnStartDownload: TNotifyEvent read FOnStartDownload
      write FOnStartDownload;
    property OnGetData: TNotifyEvent read FOnGetData write FOnGetData;

    property Files: TFiles read FFiles;
    property FilesBuf: TFiles read FFilesBuf write FFilesBuf;
  end;

implementation

{ TDownloads }

procedure TDownloads.AddDownload(N: Integer);
var
  K: Integer;
  FileDownload: TFileDownload;
  Download: TDownloadItem;
  PartManager: TPartManager;
begin
  if N = -1 then
    K := FFiles.Count
  else
  begin
    K := FFilesBuf.Items[N].ThreadID;
    if FFiles[K].FileState = fsDownloading then
      Exit;
  end;

  // �������� ���� ������������ ��� �����
  if K < FFiles.Count then
  begin
    FileDownload := TFileDownload.Create(FFiles[K].DownloadPath,
      FFiles[K].FileLink, K, FFiles[K].PartsCount, FFiles[K].FileName);

    FDatabase.GetRecord(K, Download);

    PartManager := TPartManager.Create(Download.FileName, Download.DownloadPath,
      Download.FileLink, 0, '');

    if PartManager.OpenFile(Download.DownloadPath + Download.FileName +
      '.download') then
      FileDownload.SetInfo(Download.FileName, PartManager.FileSize,
        PartManager.BytesDownloaded, Download.FileState,
        PartManager.PartsDownloaded);

    PartManager.Free;
  end
  else
    FileDownload := TFileDownload.Create(FFileAdd.DownloadPath,
      FFileAdd.FileLink, K, FFileAdd.PartsCount, '');

  FileDownload.OnDownloadEnd := DownloadEnd;
  FileDownload.OnDownloadStart := DownloadStart;
  FileDownload.OnDownloadGetInfo := DownloadInit;

  FileDownload.Start;

  if N = -1 then
  begin
    Download.DownloadPath := FFileAdd.DownloadPath;
    Download.FileLink := FFileAdd.FileLink;
    Download.PartCount := FFileAdd.PartsCount;
    Download.FileState := fsDownloading;
    Download.FileName := '';
    Download.Size := 0;

    FFiles.Add(FileDownload);
    FFilesBuf.Add(FileDownload);
    FDatabase.AddData(Download);
  end
  else
  begin
    FFiles.Items[K] := FileDownload;
    FFilesBuf.Items[N] := FileDownload;
  end;
end;

procedure TDownloads.AddDownloads;
var
  I: Integer;
  FileDownload: TFileDownload;
  Download: TDownloadItem;
begin
  for I := 0 to FFileAdd.FilesLinks.Count - 1 do
  begin
    FileDownload := TFileDownload.Create(FFileAdd.DownloadPath,
      FFileAdd.FilesLinks[I], FFiles.Count, FFileAdd.PartsCount, '');

    FileDownload.OnDownloadEnd := DownloadEnd;
    FileDownload.OnDownloadStart := DownloadStart;
    FileDownload.OnDownloadGetInfo := DownloadInit;

    FileDownload.Start;

    FFiles.Add(FileDownload);
    FFilesBuf.Add(FileDownload);

    Download.DownloadPath := FFileAdd.DownloadPath;
    Download.FileLink := FFileAdd.FilesLinks[I];
    Download.PartCount := FFileAdd.PartsCount;
    Download.FileState := fsDownloading;
    Download.FileName := '';
    Download.Size := 0;

    FDatabase.AddData(Download);
  end;
end;

constructor TDownloads.Create;
begin
  FFiles := TFiles.Create;
  FFilesBuf := TFiles.Create;
  FHistory := THistoryTable.Create;

  FFileAdd := TFileAdd.Create;
  FFileAdd.OnGetInfo := GetData;

  FDatabase := TDatabase.Create;
end;

procedure TDownloads.DeleteDownload(N: Integer);
var
  I: Integer;
  K: Integer;
begin
  K := FFilesBuf.Items[N].ThreadID;

  FFiles.Delete(K);
  FFilesBuf.Delete(N);

  for I := K to FFiles.Count - 1 do
    FFiles.Items[I].ThreadID := FFiles.Items[I].ThreadID - 1;
end;

procedure TDownloads.DeleteRecord(N: Integer);
begin
  FDatabase.DeleteData(N);
end;

destructor TDownloads.Destroy;
begin
  FFiles.Free;
  FFilesBuf.Free;

  FFileAdd.Free;

  FDatabase.Free;

  inherited;
end;

procedure TDownloads.DownDownload(N: Integer);
begin
  FFilesBuf.Exchange(N, N + 1);
end;

procedure TDownloads.DownloadEnd(Sender: TObject; FileState: TFileState;
  ThreadID: Integer);
begin
  EventEndDownload;

  DownloadInit(Sender, ThreadID);
end;

procedure TDownloads.DownloadInit(Sender: TObject; ThreadID: Integer);
var
  Download: TDownloadItem;
  HistoryItem: THistoryItem;
begin
  Download.DownloadID := FFiles[ThreadID].ThreadID;
  Download.DownloadPath := FFiles[ThreadID].DownloadPath;
  Download.FileName := FFiles[ThreadID].FileName;
  Download.FileLink := FFiles[ThreadID].FileLink;
  Download.FileState := FFiles[ThreadID].FileState;
  Download.PartCount := FFiles[ThreadID].PartsCount;
  Download.Size := FFiles[ThreadID].FileSize;

  FDatabase.UpdateData(Download);

  HistoryItem.DownloadID := FFiles[ThreadID].ThreadID;
  HistoryItem.Name := FFiles[ThreadID].FileName;
  HistoryItem.FileLink := FFiles[ThreadID].FileLink;
  HistoryItem.AddTime := FFiles[ThreadID].AddTime;
  HistoryItem.EndTime := FFiles[ThreadID].EndTime;
  HistoryItem.Size := FFiles[ThreadID].FileSize;
  HistoryItem.Path := FFiles[ThreadID].FilePath;

  FHistory.Add(HistoryItem);
end;

procedure TDownloads.DownloadStart(Sender: TObject);
begin
  EventDownloadStart;
end;

procedure TDownloads.EventEndDownload;
begin
  if Assigned(FOnEndDownload) then
    FOnEndDownload(Self);
end;

procedure TDownloads.EventGetData;
begin
  if Assigned(FOnGetData) then
    FOnGetData(Self);
end;

procedure TDownloads.GetData(Sender: TObject);
var
  I: Integer;
begin
  if FFileAdd.FilesLinks.Count = 0 then
  begin
    AddDownload(-1);
    EventGetData;
  end
  else
  begin
    AddDownloads;
    for I := 0 to FFileAdd.FilesLinks.Count - 1 do
      EventGetData;
  end;
end;

procedure TDownloads.LoadDataFromDatabase;
var
  Download: TDownloadsTable;
  I: Integer;
  FileDownload: TFileDownload;
  PartManager: TPartManager;
  pd: Cardinal;
  fs: TFileState;
begin
  Download := TDownloadsTable.Create;
  FDatabase.LoadData(Download);

  for I := 0 to Download.Count - 1 do
  begin
    FileDownload := TFileDownload.Create(Download[I].DownloadPath,
      Download[I].FileLink, I, Download[I].PartCount, Download[I].FileName);

    FileDownload.OnDownloadEnd := DownloadEnd;
    FileDownload.OnDownloadStart := DownloadStart;
    FileDownload.OnDownloadGetInfo := DownloadInit;

    FFiles.Add(FileDownload);
    FFilesBuf.Add(FileDownload);

    PartManager := TPartManager.Create(Download[I].FileName,
      Download[I].DownloadPath, Download[I].FileLink, 0, '');

    fs := Download[I].FileState;
    if fs = fsGetInfo then
      fs := fsStopped;

    if PartManager.OpenFile(Download[I].DownloadPath + Download[I].FileName +
      '.download') then
    begin
      FileDownload.SetInfo(Download[I].FileName, PartManager.FileSize,
        PartManager.BytesDownloaded, fs, PartManager.PartsDownloaded);
    end
    else
    begin
      if fs = fsComplete then
        pd := Download[I].PartCount
      else
        pd := 0;

      FileDownload.SetInfo(Download[I].FileName, Download[I].Size,
        Download[I].Size, fs, pd);
    end;

    EventGetData;

    PartManager.Free;
  end;
end;

procedure TDownloads.LoadHistory;
begin

end;

procedure TDownloads.StopDownload(N: Integer);
begin
  FFiles.Items[FFilesBuf.Items[N].ThreadID].StopDownload;
end;

procedure TDownloads.UpDownload(N: Integer);
begin
  FFilesBuf.Exchange(N, N - 1);
end;

procedure TDownloads.EventDownloadStart;
begin
  if Assigned(FOnStartDownload) then
    FOnStartDownload(Self);
end;

end.
