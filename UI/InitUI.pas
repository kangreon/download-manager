unit InitUI;

interface

uses
  Forms, TopBar, BottomBar, LeftBar, DownloadBar, ComCtrls, Downloads,
  FileDownload, SysUtils, States, ExtCtrls, VirtualTrees, Classes, Comparer,
  Generics.Defaults, FormatSettings;

type
  TUInterface = class
  private
    FTopBar: TTopBar;
    FLeftBar: TLeftBar;
    FBottomBar: TBottomBar;
    FDownloadBar: TDownloadBar;
    FTreeView: TTreeView;
    FVirtualTree: TVirtualStringTree;
    FDownloads: TDownloads;
    FTimer: TTimer;
    FTreeViewSelected: TFileState;
    FAllDownloads: Boolean;
    FSortByAscending: Boolean;
    procedure CreateComponents(Form: TForm);
    procedure ResizeTop(Form: TForm);
    procedure ResizeBottom(Form: TForm);
    procedure ResizeDownloadBar(Form: TForm);
    procedure StartDownload(Sender: TObject);
    procedure EndDownload(Sender: TObject);
    procedure PauseBtnClick(Sender: TObject);
    procedure DeleteBtnClick(Sender: TObject);
    procedure PlayBtnClick(Sender: TObject);
    procedure UpBtnClick(Sender: TObject);
    procedure DownBtnClick(Sender: TObject);
    procedure VTSAddToSelection(Sender: TBaseVirtualTree; Node: PVirtualNode);
    procedure VTSHeaderClick(Sender: TVTHeader; HitInfo: TVTHeaderHitInfo);
    procedure Update(Sender: TObject);
    procedure UpdateInfo(IsAll: Boolean);
    procedure UpdateBottomBar;
    procedure TreeViewSelectItem(Sender: TObject; N: Integer);
    procedure SrchChange(Sender: TObject; Text: string);
    procedure GetData(Sender: TObject);
    procedure GraphBtnClick(Sender: TObject);
  public
    constructor Create;
    destructor Destroy; override;

    procedure InitComponents(Form: TForm);
    procedure ResizeComponents(Form: TForm);
  end;

implementation

{ TUInterface }

procedure TUInterface.UpdateBottomBar;
var
  K: Integer;
  Node: PVirtualNode;
  I: Integer;
  FileSize: Int64;
  DownloadSpeed: Cardinal;
  RemainTime: string;
  BytesDownloaded: Int64;
  PartsDownloaded: Integer;
  AllParts: Integer;
  Time: Double;
begin
  Node := FVirtualTree.GetFirstSelected(False);
  if Assigned(Node) then
    K := Node.Index
  else
  begin
    FBottomBar.PaintInf := False;
    if not FBottomBar.PaintGraph then
      FBottomBar.Invalidate;
    Exit;
  end;

  FileSize := 0;
  DownloadSpeed := 0;
  RemainTime := '0';
  BytesDownloaded := 0;
  PartsDownloaded := 0;
  AllParts := 0;
  Time := Now;

  for I := 0 to FVirtualTree.SelectedCount - 1 do
  begin
    if FBottomBar.PaintGraph then
    begin
      DownloadSpeed := DownloadSpeed +
        Round(FDownloads.FilesBuf[K].DownloadSpeed / 1024);
      FBottomBar.GraphValue := DownloadSpeed;
    end
    else if FBottomBar.PaintInf then
    begin
      BytesDownloaded := BytesDownloaded + FDownloads.FilesBuf[K]
        .BytesDownloaded;
      FileSize := FileSize + FDownloads.FilesBuf[K].FileSize;
      PartsDownloaded := PartsDownloaded + FDownloads.FilesBuf[K]
        .PartsDownloaded;
      AllParts := AllParts + FDownloads.FilesBuf[K].PartsCount;
      DownloadSpeed := DownloadSpeed + FDownloads.FilesBuf[K].DownloadSpeed;
      Time := Time + Now;
    end;

    Node := FVirtualTree.GetNextSelected(Node, False);
    if Assigned(Node) then
      K := Node.Index;
  end;

  FBottomBar.BytesDownloaded := BytesDownloaded;
  FBottomBar.FileSize := FileSize;
  FBottomBar.PartsDownloaded := PartsDownloaded;
  FBottomBar.AllParts := AllParts;
  FBottomBar.Speed := DownloadSpeed;
  FBottomBar.Time := Time;

  if FBottomBar.PaintInf then
    FBottomBar.Invalidate;
end;

procedure TUInterface.UpdateInfo(IsAll: Boolean);
var
  I: Integer;
  ProgressPercent: Real;
  FileName: string;
  FileSize: Int64;
  FileState: TFileState;
  DownloadSpeed: Cardinal;
  RemainTime: string;
  BytesDownloaded: Int64;
begin
  for I := 0 to FDownloads.FilesBuf.Count - 1 do
  begin
    if (FDownloads.FilesBuf[I].FileState = fsDownloading) or IsAll then
    begin
      FileName := FDownloads.FilesBuf[I].FileName;
      FileSize := FDownloads.FilesBuf[I].FileSize;
      FileState := FDownloads.FilesBuf[I].FileState;
      DownloadSpeed := FDownloads.FilesBuf[I].DownloadSpeed;
      RemainTime := FDownloads.FilesBuf[I].RemainTime;
      BytesDownloaded := FDownloads.FilesBuf[I].BytesDownloaded;

      if FileSize <> 0 then
        ProgressPercent := BytesDownloaded / FileSize
      else
        ProgressPercent := 0;

      FDownloadBar.AddInfo(FVirtualTree, I, ProgressPercent, FileName,
        FormatByteSize(FileSize), FileState, FormatByteSize(DownloadSpeed) +
        '/c', RemainTime);
    end;
  end;

  if FDownloads.FilesBuf.Count = 0 then
    FVirtualTree.Refresh;

  UpdateBottomBar;
end;

procedure TUInterface.Update(Sender: TObject);
begin
  UpdateInfo(False);
end;

constructor TUInterface.Create;
begin
  FSortByAscending := True;

  FDownloads := TDownloads.Create;
  FDownloads.OnStartDownload := StartDownload;
  FDownloads.OnEndDownload := EndDownload;
  FDownloads.OnGetData := GetData;

  FTimer := TTimer.Create(nil);
  FTimer.Enabled := True;
  FTimer.Interval := 200;
  FTimer.OnTimer := Update;

  FAllDownloads := True;
end;

procedure TUInterface.CreateComponents(Form: TForm);
begin
  FTopBar := TTopBar.Create(Form);
  FTopBar.Parent := Form;
  FTopBar.OnPauseBtnClick := PauseBtnClick;
  FTopBar.OnDeleteBtnClick := DeleteBtnClick;
  FTopBar.OnPlayBtnClick := PlayBtnClick;
  FTopBar.OnUpBtnClick := UpBtnClick;
  FTopBar.OnDownBtnClick := DownBtnClick;
  FTopBar.OnSrchChange := SrchChange;
  FTopBar.Left := 145;
  FTopBar.Width := Form.ClientWidth - 145;
  FTopBar.Top := 0;
  FTopBar.Height := 44;

  FBottomBar := TBottomBar.Create(Form);
  FBottomBar.Parent := Form;
  FBottomBar.OnGraphBtnClick := GraphBtnClick;
  FBottomBar.Width := Form.ClientWidth - 145;
  FBottomBar.Left := 145;
  FBottomBar.Top := Form.ClientHeight - 75;
  FBottomBar.Height := 75;

  FTreeView := TTreeView.Create(Form);
  FTreeView.Parent := Form;
  FLeftBar := TLeftBar.Create(FTreeView);
  FLeftBar.OnSelectItem := TreeViewSelectItem;

  FVirtualTree := TVirtualStringTree.Create(Form);
  FVirtualTree.Parent := Form;
  FVirtualTree.OnAddToSelection := VTSAddToSelection;
  FVirtualTree.OnHeaderClick := VTSHeaderClick;
  FDownloadBar := TDownloadBar.Create(FVirtualTree, Form.ClientWidth,
    Form.ClientHeight);
end;

procedure TUInterface.DeleteBtnClick(Sender: TObject);
var
  N: Integer;
  I: Integer;
  Node: PVirtualNode;
begin
  Node := FVirtualTree.GetFirstSelected(False);

  if not Assigned(Node) then
    Exit;

  for I := 0 to FVirtualTree.SelectedCount - 1 do
  begin
    N := Node.Index;

    FVirtualTree.DeleteNode(Node, True);

    FDownloads.DeleteRecord(FDownloads.FilesBuf[N].ThreadID);

    FDownloads.StopDownload(N);
    FDownloads.DeleteDownload(N);

    Node := FVirtualTree.GetNextSelected(Node, False);
  end;

  UpdateInfo(True);
end;

destructor TUInterface.Destroy;
var
  I: Integer;
begin
  for I := 0 to FDownloads.Files.Count - 1 do
    FDownloads.StopDownload(I);

  inherited;
end;

procedure TUInterface.DownBtnClick(Sender: TObject);
var
  N: Integer;
  I: Integer;
  Node: PVirtualNode;
begin
  Node := FVirtualTree.GetLast(nil, False);
  if not Assigned(Node) then
    Exit;

  if vsSelected in Node.States then
    Exit;

  Node := FVirtualTree.GetPreviousSelected(Node, False);

  for I := 0 to FVirtualTree.SelectedCount - 1 do
  begin
    N := Node.Index;

    if I = 0 then
      FVirtualTree.Selected[Node] := False
    else
      FVirtualTree.Selected[Node] := False;

    FVirtualTree.Selected[FVirtualTree.GetNext(Node, False)] := True;

    FDownloads.DownDownload(N);
    Node := FVirtualTree.GetPreviousSelected(Node, False);
  end;

  UpdateInfo(True);
end;

procedure TUInterface.EndDownload(Sender: TObject);
begin
  FVirtualTree.Refresh;

  UpdateInfo(True);
end;

procedure TUInterface.InitComponents(Form: TForm);
begin
  CreateComponents(Form);
  FDownloads.LoadDataFromDatabase;
end;

procedure TUInterface.PauseBtnClick(Sender: TObject);
var
  N: Integer;
  I: Integer;
  Node: PVirtualNode;
begin
  Node := FVirtualTree.GetFirstSelected(False);

  if not Assigned(Node) then
    Exit;

  for I := 0 to FVirtualTree.SelectedCount - 1 do
  begin
    N := Node.Index;
    FDownloads.StopDownload(N);
    Node := FVirtualTree.GetNextSelected(Node, False);
  end;
end;

procedure TUInterface.PlayBtnClick(Sender: TObject);
var
  N: Integer;
  I: Integer;
  Node: PVirtualNode;
begin
  Node := FVirtualTree.GetFirstSelected(False);

  if not Assigned(Node) then
    Exit;

  for I := 0 to FVirtualTree.SelectedCount - 1 do
  begin
    N := Node.Index;
    FDownloads.AddDownload(N);
    Node := FVirtualTree.GetNextSelected(Node, False);
  end;
end;

procedure TUInterface.ResizeBottom(Form: TForm);
begin
  FBottomBar.Width := Form.ClientWidth - 145;
  FBottomBar.Left := 145;
  FBottomBar.Top := Form.ClientHeight - 75;
  FBottomBar.Height := 75;
end;

procedure TUInterface.ResizeComponents(Form: TForm);
begin
  ResizeTop(Form);
  ResizeBottom(Form);
  ResizeDownloadBar(Form);
end;

procedure TUInterface.ResizeDownloadBar(Form: TForm);
begin
  FDownloadBar.Resize(FVirtualTree, Form.ClientWidth, Form.ClientHeight);
end;

procedure TUInterface.ResizeTop(Form: TForm);
begin
  FTopBar.Width := Form.ClientWidth - 145;
  FTopBar.Left := 145;
  FTopBar.Top := 0;
  FTopBar.Height := 44;
end;

procedure TUInterface.VTSAddToSelection(Sender: TBaseVirtualTree;
  Node: PVirtualNode);
begin
  if not FBottomBar.PaintGraph then
    FBottomBar.PaintInf := True
  else
  begin
    FBottomBar.SetGraphToZero;
    FBottomBar.PaintInf := False;
  end;
end;

procedure TUInterface.VTSHeaderClick(Sender: TVTHeader;
  HitInfo: TVTHeaderHitInfo);
var
  Comparison: TComparison<TFileDownload>;
begin
  if FSortByAscending then
    case HitInfo.Column of
      0:
        Comparison := CompareByAscendingFileName;
      1:
        Comparison := CompareByAscendingFileSize;
      2:
        Comparison := CompareByAscendingBytesDownloaded;
      3:
        Comparison := CompareByAscendingDownloadSpeed;
      4:
        Comparison := CompareByAscendingTimeRemain;
    end;

  if not FSortByAscending then
    case HitInfo.Column of
      0:
        Comparison := CompareByDescendingFileName;
      1:
        Comparison := CompareByDescendingFileSize;
      2:
        Comparison := CompareByDescendingBytesDownloaded;
      3:
        Comparison := CompareByDescendingDownloadSpeed;
      4:
        Comparison := CompareByDescendingTimeRemain;
    end;

  FDownloads.FilesBuf.Sort(TComparer<TFileDownload>.Construct(Comparison));

  FSortByAscending := not FSortByAscending;

  UpdateInfo(True);
end;

procedure TUInterface.SrchChange(Sender: TObject; Text: string);
begin
  FDownloadBar.FindDownload(FVirtualTree, Text);
end;

procedure TUInterface.StartDownload(Sender: TObject);
begin
  FTimer.Enabled := True;
end;

procedure TUInterface.TreeViewSelectItem(Sender: TObject; N: Integer);
var
  I: Integer;
  FileName: string;
  FileSize: Int64;
  FileState: TFileState;
  DownloadSpeed: Cardinal;
  RemainTime: string;
begin
  FTimer.Enabled := False;
  case N of
    0:
      FAllDownloads := True;
    1:
      begin
        FTreeViewSelected := fsDownloading;
        FAllDownloads := False;
      end;
    2:
      begin
        FTreeViewSelected := fsStopped;
        FAllDownloads := False;
      end;
    3:
      begin
        FTreeViewSelected := fsComplete;
        FAllDownloads := False;
      end;
    4:
      begin
        FTreeViewSelected := fsError;
        FAllDownloads := False;
      end;
  end;

  FVirtualTree.Clear;
  FDownloads.FilesBuf.Clear;

  for I := 0 to FDownloads.Files.Count - 1 do
  begin
    FileName := FDownloads.Files.Items[I].FileName;
    FileSize := FDownloads.Files.Items[I].FileSize;
    FileState := FDownloads.Files.Items[I].FileState;
    DownloadSpeed := FDownloads.Files.Items[I].DownloadSpeed;
    RemainTime := FDownloads.Files.Items[I].RemainTime;

    if FAllDownloads then
    begin
      FDownloadBar.AddDownload(FVirtualTree, FileName, FormatByteSize(FileSize),
        FileState, FormatByteSize(DownloadSpeed) + '/c', RemainTime);
      FDownloads.FilesBuf.Add(FDownloads.Files.Items[I]);
    end
    else if FTreeViewSelected = FileState then
    begin
      FDownloadBar.AddDownload(FVirtualTree, FileName, FormatByteSize(FileSize),
        FileState, FormatByteSize(DownloadSpeed) + '/c', RemainTime);
      FDownloads.FilesBuf.Add(FDownloads.Files.Items[I]);
    end;
  end;

  FTimer.Enabled := True;
  UpdateInfo(True);
end;

procedure TUInterface.UpBtnClick(Sender: TObject);
var
  N: Integer;
  I: Integer;
  Node: PVirtualNode;
begin
  Node := FVirtualTree.GetFirstSelected(False);

  if not Assigned(Node) then
    Exit;

  if Node.Index = 0 then
    Exit;

  for I := 0 to FVirtualTree.SelectedCount - 1 do
  begin
    N := Node.Index;

    if I = 0 then
      FVirtualTree.Selected[FVirtualTree.GetFirstSelected(False)] := False
    else
      FVirtualTree.Selected[Node] := False;

    FVirtualTree.Selected[FVirtualTree.GetPrevious(Node, False)] := True;

    FDownloads.UpDownload(N);

    Node := FVirtualTree.GetNextSelected(Node, False);
  end;

  UpdateInfo(True);
end;

procedure TUInterface.GetData(Sender: TObject);
begin
  FDownloadBar.AddDownload(FVirtualTree, '', '', fsDownloading, '0 �/�', '0');
  UpdateInfo(True);
end;

procedure TUInterface.GraphBtnClick(Sender: TObject);
begin
  VTSAddToSelection(FVirtualTree, FVirtualTree.GetFirstSelected(False));
  UpdateBottomBar;
end;

end.
