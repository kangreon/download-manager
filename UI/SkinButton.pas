unit SkinButton;

interface

uses
  System.SysUtils, Windows, System.Classes, Vcl.Controls, Graphics, PngImage,
  Messages;

type
  TButtonState = (bsNone, bsActive, bsDown);

  TSkinButton = class(TGraphicControl)
  private
    FButtonState: TButtonState;
    FImage: TPngImage;
    FPicture: TPicture;
    procedure SetPicture(const Value: TPicture);
    procedure UpdateImage(Clear: Boolean);
    procedure PictureOnChange(Sender: TObject);
    { Private declarations }
  protected
    procedure Paint; override;
    procedure WndProc(var Message: TMessage); override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  published
    property Anchors;
    property Constraints;
    property DragCursor;
    property DragKind;
    property DragMode;
    property Enabled;
    property Picture: TPicture read FPicture write SetPicture;
    property Visible;
    property OnClick;
    property OnContextPopup;
    property OnDblClick;
    property OnDragDrop;
    property OnDragOver;
    property OnEndDock;
    property OnEndDrag;
    property OnGesture;
    property OnMouseActivate;
    property OnMouseDown;
    property OnMouseEnter;
    property OnMouseLeave;
    property OnMouseMove;
    property OnMouseUp;
    property OnStartDock;
    property OnStartDrag;
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('SkinControl', [TSkinButton]);
end;

{ TSkinButton }

constructor TSkinButton.Create(AOwner: TComponent);
begin
  inherited;

  FPicture := TPicture.Create;
  FPicture.OnChange :=  PictureOnChange;
  FPicture.RegisterFileFormat('png', 'PNG File', TPngImage);

  FImage := TPngImage.Create;
  FButtonState := bsNone;
end;

destructor TSkinButton.Destroy;
begin
  FPicture.Free;
  FImage.Free;
  inherited;
end;

procedure TSkinButton.PictureOnChange(Sender: TObject);
begin
  if FPicture.Width = 0 then
  begin
    UpdateImage(True);
  end
  else
    UpdateImage(False);
end;

procedure TSkinButton.Paint;
var
  w, l, h: Integer;
begin
  inherited;
  if FImage.Width > 0 then
  begin
    h := FImage.Height;
    w := FImage.Width div 4;

    if (ClientWidth <> w) or (ClientHeight <> h) then
    begin
      if Align <> alNone then
        Align := alNone;

      ClientWidth := w;
      ClientHeight := h;
      Exit;
    end;

    l := 0;
    case FButtonState of
      bsActive: l := w;
      bsDown: l := w * 2;
    end;

    if not Enabled then
      l := w * 3;

    BitBlt(Canvas.Handle, 0, 0, w, h, FImage.Canvas.Handle, l, 0, SRCCOPY);
  end;
end;

procedure TSkinButton.SetPicture(const Value: TPicture);
begin
  if Value.Width = 0 then
  begin
    FPicture.Assign(Value);
    UpdateImage(True);
  end
  else
  begin
    if (not (Value.Graphic is TPngImage)) and
      (not (Value.Graphic is TBitmap)) then
    begin
      MessageBox(0, '�������������� ������ PNG �����������.', '������',
        MB_ICONWARNING);
    end
    else
    begin
      FPicture.Assign(Value);
      UpdateImage(False);
    end;
  end;
end;

procedure TSkinButton.UpdateImage(Clear: Boolean);
begin
  if Clear then
  begin
    FImage.Free;
    FImage := TPngImage.Create;
  end
  else
  begin
    FImage.Assign(FPicture.Graphic);
  end;

  Invalidate;
end;

procedure TSkinButton.WndProc(var Message: TMessage);
begin
  inherited;
  case Message.Msg of
    CM_MOUSEENTER:
      begin
        if FButtonState = bsNone then
        begin
          FButtonState := bsActive;
          Invalidate;
        end;

      end;
    CM_MOUSELEAVE:
      begin
        FButtonState := bsNone;
        Invalidate;
      end;
    WM_LBUTTONDOWN, WM_LBUTTONDBLCLK:
      begin
        FButtonState := bsDown;
        Invalidate;
      end;
    WM_LBUTTONUP:
      begin
        if FButtonState = bsDown then
        begin
          FButtonState := bsActive;
          Invalidate;
        end;
      end;
  end;
end;

end.
