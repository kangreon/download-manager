unit Comparer;

interface

uses
  FileDownload, Generics.Defaults, Generics.Collections, SysUtils;

function CompareByAscendingFileName(const F1, F2: TFileDownload): Integer;
function CompareByAscendingFileSize(const F1, F2: TFileDownload): Integer;
function CompareByAscendingBytesDownloaded(const F1, F2: TFileDownload)
  : Integer;
function CompareByAscendingDownloadSpeed(const F1, F2: TFileDownload): Integer;
function CompareByAscendingTimeRemain(const F1, F2: TFileDownload): Integer;

function CompareByDescendingFileName(const F1, F2: TFileDownload): Integer;
function CompareByDescendingFileSize(const F1, F2: TFileDownload): Integer;
function CompareByDescendingBytesDownloaded(const F1,
  F2: TFileDownload): Integer;
function CompareByDescendingDownloadSpeed(const F1, F2: TFileDownload): Integer;
function CompareByDescendingTimeRemain(const F1, F2: TFileDownload): Integer;

implementation

function CompareByAscendingFileName(const F1, F2: TFileDownload): Integer;
begin
  Result := AnsiCompareStr(F1.FileName, F2.FileName);
end;

function CompareByAscendingFileSize(const F1, F2: TFileDownload): Integer;
begin
  Result := F1.FileSize - F2.FileSize;
end;

function CompareByAscendingBytesDownloaded(const F1, F2: TFileDownload)
  : Integer;
begin
  Result := F1.BytesDownloaded - F2.BytesDownloaded;
end;

function CompareByAscendingDownloadSpeed(const F1, F2: TFileDownload): Integer;
begin
  Result := F1.DownloadSpeed - F2.DownloadSpeed;
end;

function CompareByAscendingTimeRemain(const F1, F2: TFileDownload): Integer;
begin
  Result := AnsiCompareStr(F1.RemainTime, F2.RemainTime);
end;

function CompareByDescendingFileName(const F1, F2: TFileDownload): Integer;
begin
  Result := AnsiCompareStr(F2.FileName, F1.FileName);
end;

function CompareByDescendingFileSize(const F1, F2: TFileDownload): Integer;
begin
  Result := F2.FileSize - F1.FileSize;
end;

function CompareByDescendingBytesDownloaded(const F1,
  F2: TFileDownload): Integer;
begin
  Result := F2.BytesDownloaded - F1.BytesDownloaded;
end;

function CompareByDescendingDownloadSpeed(const F1, F2: TFileDownload): Integer;
begin
  Result := F2.DownloadSpeed - F1.DownloadSpeed;
end;

function CompareByDescendingTimeRemain(const F1, F2: TFileDownload): Integer;
begin
  Result := AnsiCompareStr(F2.FileName, F1.FileName);
end;

end.
