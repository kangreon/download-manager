unit TopBar;

interface

uses
  Controls, Classes, Windows, SkinButton, PngImage, StdCtrls, fmAddFile,
  fmSettings;

const
  BarHeight = 44;
  MarginLeft = 145;
  MarginButton = 10;
  MarginButtonTop = 6;
  IconSize = 32;
  PlayIconSize = 27;
  PauseIconSize = 22;
  ArrowIconSize = 24;
  DelimeterWidth = 1;

type
  TProcSrchChange = procedure(Sender: TObject; Text: string) of object;

  TTopBar = class(TCustomControl)
  private
    FSearch: TEdit;
    FAddBtn: TSkinButton;
    FDeleteBtn: TSkinButton;
    FPlayBtn: TSkinButton;
    FPauseBtn: TSkinButton;
    FUpBtn: TSkinButton;
    FDownBtn: TSkinButton;
    FSettingsBtn: TSkinButton;
    FPosition: Integer;
    FOnPauseBtnClick: TNotifyEvent;
    FOnDeleteBtnClick: TNotifyEvent;
    FOnPlayBtnClick: TNotifyEvent;
    FOnUpBtnClick: TNotifyEvent;
    FOnDownBtnClick: TNotifyEvent;
    FOnSrchChange: TProcSrchChange;
    procedure PaintBackgorund;
    procedure PaintLine(X: Integer);
    procedure SetAddBtn(X: Integer);
    procedure SetDeleteBtn(X: Integer);
    procedure SetPlayBtn(X: Integer);
    procedure SetPauseBtn(X: Integer);
    procedure SetUpBtn(X: Integer);
    procedure SetDownBtn(X: Integer);
    procedure SetSettingsBtn(X: Integer);
    procedure AddBtnClick(Sender: TObject);
    procedure EventPauseBtnClick(Sender: TObject);
    procedure PauseBtnClick(Sender: TObject);
    procedure EventDeleteBtnClick(Sender: TObject);
    procedure DeleteBtnClick(Sender: TObject);
    procedure EventPlayBtnClick(Sender: TObject);
    procedure PlayBtnClick(Sender: TObject);
    procedure EventUpBtnClick(Sender: TObject);
    procedure UpBtnClick(Sender: TObject);
    procedure EventDownBtnClick(Sender: TObject);
    procedure DownBtnClick(Sender: TObject);
    procedure SettingsBtnClick(Sender: TObject);
    procedure EventSrchChange(Sender: TObject; Text: string);
    procedure SrchChange(Sender: TObject);
  protected
    procedure CreateWnd; override;
    procedure Paint; override;
    procedure Resize; override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    property OnPauseBtnClick: TNotifyEvent read FOnPauseBtnClick
      write FOnPauseBtnClick;
    property OnDeleteBtnClick: TNotifyEvent read FOnDeleteBtnClick
      write FOnDeleteBtnClick;
    property OnPlayBtnClick: TNotifyEvent read FOnPlayBtnClick
      write FOnPlayBtnClick;
    property OnUpBtnClick: TNotifyEvent read FOnUpBtnClick write FOnUpBtnClick;
    property OnDownBtnClick: TNotifyEvent read FOnDownBtnClick
      write FOnDownBtnClick;
    property OnSrchChange: TProcSrchChange read FOnSrchChange
      write FOnSrchChange;
  end;

implementation

{ TTopBar }

procedure TTopBar.AddBtnClick(Sender: TObject);
begin
  AddFileForm.ShowModal;
end;

constructor TTopBar.Create(AOwner: TComponent);
begin
  inherited;

end;

procedure TTopBar.CreateWnd;
begin
  inherited;

  FPosition := MarginButton;
  SetAddBtn(FPosition);

  FPosition := FPosition + IconSize + MarginButton + DelimeterWidth +
    MarginButton;
  SetDeleteBtn(FPosition);

  FPosition := FPosition + IconSize + MarginButton + DelimeterWidth +
    MarginButton;
  SetPlayBtn(FPosition);

  FPosition := FPosition + PlayIconSize + MarginButton;
  SetPauseBtn(FPosition);

  FPosition := FPosition + PauseIconSize + MarginButton + DelimeterWidth +
    MarginButton;
  SetUpBtn(FPosition);

  FPosition := FPosition + ArrowIconSize + MarginButton;
  SetDownBtn(FPosition);

  SetSettingsBtn(ClientWidth - MarginButton - IconSize);

  FSearch := TEdit.Create(Self);
  FSearch.Parent := Self;
  FSearch.OnChange := SrchChange;

  FPosition := FPosition + ArrowIconSize + MarginButton + DelimeterWidth +
    MarginButton;
  FSearch.Left := FPosition;
  FSearch.Top := (BarHeight - FSearch.Height) div 2;

  SetFocus;
end;

procedure TTopBar.DeleteBtnClick(Sender: TObject);
begin
  EventDeleteBtnClick(Sender)
end;

destructor TTopBar.Destroy;
begin

  inherited;
end;

procedure TTopBar.DownBtnClick(Sender: TObject);
begin
  EventDownBtnClick(Sender);
end;

procedure TTopBar.EventDeleteBtnClick(Sender: TObject);
begin
  if Assigned(FOnDeleteBtnClick) then
    FOnDeleteBtnClick(Sender);
end;

procedure TTopBar.EventDownBtnClick(Sender: TObject);
begin
  if Assigned(FOnDownBtnClick) then
    FOnDownBtnClick(Sender);
end;

procedure TTopBar.EventPauseBtnClick(Sender: TObject);
begin
  if Assigned(FOnPauseBtnClick) then
    FOnPauseBtnClick(Sender);
end;

procedure TTopBar.EventPlayBtnClick(Sender: TObject);
begin
  if Assigned(FOnPlayBtnClick) then
    FOnPlayBtnClick(Sender);
end;

procedure TTopBar.EventSrchChange(Sender: TObject; Text: string);
begin
  if Assigned(FOnSrchChange) then
    FOnSrchChange(Sender, Text);
end;

procedure TTopBar.EventUpBtnClick(Sender: TObject);
begin
  if Assigned(FOnUpBtnClick) then
    FOnUpBtnClick(Sender);
end;

procedure TTopBar.Paint;
begin
  inherited;

  PaintBackgorund;

  FPosition := MarginButton + IconSize + MarginButton;
  PaintLine(FPosition);

  FPosition := FPosition + DelimeterWidth + MarginButton + IconSize +
    MarginButton;
  PaintLine(FPosition);

  FPosition := FPosition + DelimeterWidth + MarginButton + PlayIconSize +
    MarginButton + PauseIconSize + MarginButton;
  PaintLine(FPosition);

  FPosition := FPosition + DelimeterWidth + MarginButton + ArrowIconSize +
    MarginButton + ArrowIconSize + MarginButton;
  PaintLine(FPosition);

  FPosition := FPosition + DelimeterWidth + MarginButton + FSearch.Width +
    MarginButton;
  PaintLine(FPosition);

  PaintLine(ClientWidth - MarginButton - IconSize - MarginButton);
  FSettingsBtn.Left := ClientWidth - MarginButton - IconSize;
end;

procedure TTopBar.PaintBackgorund;
begin
  Canvas.Brush.Color := RGB(240, 240, 240);
  Canvas.FillRect(Canvas.ClipRect);
end;

procedure TTopBar.PaintLine(X: Integer);
var
  Line: TPngImage;
begin
  Line := TPngImage.Create;
  Line.LoadFromResourceName(0, 'delim');

  Canvas.Draw(X, 6, Line);

  Line.Free;
end;

procedure TTopBar.PauseBtnClick(Sender: TObject);
begin
  EventPauseBtnClick(Sender);
end;

procedure TTopBar.PlayBtnClick(Sender: TObject);
begin
  EventPlayBtnClick(Sender);
end;

procedure TTopBar.Resize;
begin
  inherited;

end;

procedure TTopBar.SetAddBtn(X: Integer);
var
  PngImage: TPngImage;
begin
  FAddBtn := TSkinButton.Create(Self);
  FAddBtn.Parent := Self;
  FAddBtn.Left := X;
  FAddBtn.Top := MarginButtonTop;
  FAddBtn.Visible := True;

  PngImage := TPngImage.Create;
  PngImage.LoadFromResourceName(0, 'add');
  FAddBtn.Picture.Graphic := PngImage;

  FAddBtn.OnClick := AddBtnClick;
end;

procedure TTopBar.SetDeleteBtn(X: Integer);
var
  PngImage: TPngImage;
begin
  FDeleteBtn := TSkinButton.Create(Self);
  FDeleteBtn.Parent := Self;
  FDeleteBtn.Left := X;
  FDeleteBtn.Top := MarginButtonTop;
  FDeleteBtn.Visible := True;

  PngImage := TPngImage.Create;
  PngImage.LoadFromResourceName(0, 'delete');
  FDeleteBtn.Picture.Graphic := PngImage;

  FDeleteBtn.OnClick := DeleteBtnClick;
end;

procedure TTopBar.SetDownBtn(X: Integer);
var
  PngImage: TPngImage;
begin
  FDownBtn := TSkinButton.Create(Self);
  FDownBtn.Parent := Self;
  FDownBtn.Left := X;
  FDownBtn.Top := MarginButtonTop;
  FDownBtn.Visible := True;

  PngImage := TPngImage.Create;
  PngImage.LoadFromResourceName(0, 'down');
  FDownBtn.Picture.Graphic := PngImage;

  FDownBtn.OnClick := DownBtnClick;
end;

procedure TTopBar.SetPauseBtn(X: Integer);
var
  PngImage: TPngImage;
begin
  FPauseBtn := TSkinButton.Create(Self);
  FPauseBtn.Parent := Self;
  FPauseBtn.Left := X;
  FPauseBtn.Top := MarginButtonTop;
  FPauseBtn.Visible := True;

  PngImage := TPngImage.Create;
  PngImage.LoadFromResourceName(0, 'pause');
  FPauseBtn.Picture.Graphic := PngImage;

  FPauseBtn.OnClick := PauseBtnClick;
end;

procedure TTopBar.SetPlayBtn(X: Integer);
var
  PngImage: TPngImage;
begin
  FPlayBtn := TSkinButton.Create(Self);
  FPlayBtn.Parent := Self;
  FPlayBtn.Left := X;
  FPlayBtn.Top := MarginButtonTop;
  FPlayBtn.Visible := True;

  PngImage := TPngImage.Create;
  PngImage.LoadFromResourceName(0, 'play');
  FPlayBtn.Picture.Graphic := PngImage;

  FPlayBtn.OnClick := PlayBtnClick;
end;

procedure TTopBar.SetSettingsBtn(X: Integer);
var
  PngImage: TPngImage;
begin
  FSettingsBtn := TSkinButton.Create(Self);
  FSettingsBtn.Parent := Self;
  FSettingsBtn.Top := MarginButtonTop;
  FSettingsBtn.Left := ClientWidth - MarginButton - IconSize;
  FSettingsBtn.Visible := True;

  PngImage := TPngImage.Create;
  PngImage.LoadFromResourceName(0, 'settings');
  FSettingsBtn.Picture.Graphic := PngImage;

  FSettingsBtn.OnClick := SettingsBtnClick;
end;

procedure TTopBar.SettingsBtnClick(Sender: TObject);
begin
  SettingsForm.ShowModal;
end;

procedure TTopBar.SetUpBtn(X: Integer);
var
  PngImage: TPngImage;
begin
  FUpBtn := TSkinButton.Create(Self);
  FUpBtn.Parent := Self;
  FUpBtn.Left := X;
  FUpBtn.Top := MarginButtonTop;
  FUpBtn.Visible := True;

  PngImage := TPngImage.Create;
  PngImage.LoadFromResourceName(0, 'up');
  FUpBtn.Picture.Graphic := PngImage;

  FUpBtn.OnClick := UpBtnClick;
end;

procedure TTopBar.SrchChange(Sender: TObject);
begin
  EventSrchChange(Sender, FSearch.Text);
end;

procedure TTopBar.UpBtnClick(Sender: TObject);
begin
  EventUpBtnClick(Sender);
end;

end.
