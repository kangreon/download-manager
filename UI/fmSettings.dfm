object SettingsForm: TSettingsForm
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1080' '#1087#1088#1086#1075#1088#1072#1084#1084#1099
  ClientHeight = 408
  ClientWidth = 567
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 155
    Top = 0
    Width = 412
    Height = 367
    ActivePage = TabSheet4
    Align = alClient
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'TabSheet1'
      TabVisible = False
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 404
        Height = 357
        Align = alClient
        Caption = #1054#1073#1097#1080#1077' '#1085#1072#1089#1090#1088#1086#1081#1082#1080': '
        TabOrder = 0
        object CheckBox1: TCheckBox
          Left = 16
          Top = 24
          Width = 177
          Height = 17
          Caption = #1047#1072#1087#1091#1089#1082#1072#1090#1100' '#1087#1088#1080' '#1089#1090#1072#1088#1090#1077' Windows'
          TabOrder = 0
        end
        object CheckBox2: TCheckBox
          Left = 16
          Top = 47
          Width = 249
          Height = 17
          Caption = #1057#1074#1086#1088#1072#1095#1080#1074#1072#1090#1100' '#1074' '#1090#1088#1077#1081' '#1087#1088#1080' '#1079#1072#1082#1088#1099#1090#1080#1080' '#1087#1088#1086#1075#1088#1072#1084#1084#1099
          TabOrder = 1
        end
        object CheckBox3: TCheckBox
          Left = 16
          Top = 70
          Width = 217
          Height = 17
          Caption = #1054#1090#1086#1073#1088#1072#1078#1072#1090#1100' '#1080#1082#1086#1085#1082#1091' '#1087#1088#1086#1075#1088#1072#1084#1084#1099' '#1074' '#1090#1088#1077#1077
          TabOrder = 2
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'TabSheet2'
      ImageIndex = 1
      TabVisible = False
      object GroupBox2: TGroupBox
        Left = 0
        Top = 0
        Width = 404
        Height = 357
        Align = alClient
        Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1080' '#1080#1085#1090#1077#1075#1088#1072#1094#1080#1080': '
        TabOrder = 0
        object CheckBox4: TCheckBox
          Left = 16
          Top = 24
          Width = 225
          Height = 17
          Caption = #1048#1085#1090#1077#1075#1088#1072#1094#1080#1103' '#1074' Microsoft Internet Explorer'
          TabOrder = 0
        end
        object CheckBox5: TCheckBox
          Left = 16
          Top = 47
          Width = 121
          Height = 17
          Caption = #1048#1085#1090#1077#1075#1088#1072#1094#1080#1103' '#1074' Opera'
          TabOrder = 1
        end
        object CheckBox6: TCheckBox
          Left = 16
          Top = 70
          Width = 185
          Height = 17
          Caption = #1048#1085#1090#1077#1075#1088#1072#1094#1080#1103' '#1074' Mozilla Firefox'
          TabOrder = 2
        end
        object CheckBox7: TCheckBox
          Left = 16
          Top = 93
          Width = 225
          Height = 17
          Caption = #1048#1085#1090#1077#1075#1088#1072#1094#1080#1103' '#1074' Google Chrome'
          TabOrder = 3
        end
        object CheckBox8: TCheckBox
          Left = 16
          Top = 116
          Width = 225
          Height = 17
          Caption = #1048#1085#1090#1077#1075#1088#1072#1094#1080#1103' '#1074' '#1071#1085#1076#1077#1082#1089'.'#1041#1088#1072#1091#1079#1077#1088
          TabOrder = 4
        end
      end
      object CheckBox16: TCheckBox
        Left = 16
        Top = 139
        Width = 185
        Height = 17
        Caption = #1054#1090#1089#1083#1077#1078#1080#1074#1072#1090#1100' '#1073#1091#1092#1092#1077#1088' '#1086#1073#1084#1077#1085#1072
        TabOrder = 1
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'TabSheet3'
      ImageIndex = 2
      TabVisible = False
      object GroupBox3: TGroupBox
        Left = 0
        Top = 0
        Width = 404
        Height = 357
        Align = alClient
        Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1080' '#1089#1086#1077#1076#1080#1085#1077#1085#1080#1103': '
        TabOrder = 0
        object Label1: TLabel
          Left = 16
          Top = 24
          Width = 88
          Height = 13
          Caption = #1058#1080#1087' '#1089#1086#1077#1076#1080#1085#1077#1085#1080#1103': '
        end
        object Label2: TLabel
          Left = 16
          Top = 112
          Width = 59
          Height = 13
          Caption = 'User-Agent:'
        end
        object ComboBox1: TComboBox
          Left = 16
          Top = 43
          Width = 369
          Height = 21
          Style = csDropDownList
          ItemIndex = 0
          TabOrder = 0
          Text = '28.8K/33.6K'
          Items.Strings = (
            '28.8K/33.6K'
            '56K/Mobile-GPRS'
            '64K/128K'
            'Mobile 3G (EDGE/UMTS)'
            '256Kb/512Kb DSL/Cable/Other'
            '1Mb DSL/Cable/Other'
            '2Mb/4Mb DSL/Cable/Other'
            '10Mb DSL/LTE/Cable/Other'
            '20Mb '#1080' '#1073#1086#1083#1077#1077' DSL/Cable/Other')
        end
        object Button4: TButton
          Left = 16
          Top = 70
          Width = 145
          Height = 25
          Caption = #1042#1099#1073#1088#1072#1090#1100' '#1072#1074#1090#1086#1084#1072#1090#1080#1095#1077#1089#1082#1080
          TabOrder = 1
        end
        object ComboBox2: TComboBox
          Left = 16
          Top = 131
          Width = 369
          Height = 21
          Style = csDropDownList
          ItemIndex = 0
          TabOrder = 2
          Text = 'Microsoft Internet Explorer 9.0'
          Items.Strings = (
            'Microsoft Internet Explorer 9.0'
            'Microsoft Internet Explorer 11.0 (Windows 7)'
            'Microsoft Internet Explorer 11.0 (Windows 8.1)'
            'Mozilla Firefox 4.0'
            'Opera 11.10'
            'Safari 5.0.5')
        end
      end
    end
    object TabSheet4: TTabSheet
      Caption = 'TabSheet4'
      ImageIndex = 3
      TabVisible = False
      object GroupBox4: TGroupBox
        Left = 0
        Top = 0
        Width = 404
        Height = 357
        Align = alClient
        Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1080' '#1079#1072#1075#1088#1091#1079#1086#1082': '
        TabOrder = 0
        ExplicitLeft = 2
        ExplicitTop = -3
        object Label3: TLabel
          Left = 16
          Top = 263
          Width = 137
          Height = 13
          Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086' '#1087#1077#1088#1077#1079#1072#1087#1091#1089#1082#1086#1074':'
        end
        object CheckBox9: TCheckBox
          Left = 16
          Top = 24
          Width = 377
          Height = 17
          Caption = #1040#1089#1089#1086#1094#1080#1080#1088#1086#1074#1072#1090#1100' '#1088#1072#1089#1096#1080#1088#1077#1085#1080#1103' '#1092#1072#1081#1083#1086#1074' '#1080' '#1087#1072#1087#1082#1091' '#1079#1072#1075#1088#1091#1079#1086#1082' '#1076#1083#1103' '#1101#1090#1080#1093' '#1092#1072#1081#1083#1086#1074
          TabOrder = 0
        end
        object ListView1: TListView
          Left = 16
          Top = 47
          Width = 377
          Height = 150
          Columns = <
            item
              Caption = #1056#1072#1089#1096#1080#1088#1077#1085#1080#1077
            end
            item
              Caption = #1055#1072#1087#1082#1072
            end>
          GridLines = True
          ReadOnly = True
          RowSelect = True
          TabOrder = 1
          ViewStyle = vsReport
        end
        object Button5: TButton
          Left = 16
          Top = 203
          Width = 75
          Height = 25
          Caption = #1044#1086#1073#1072#1074#1080#1090#1100
          TabOrder = 2
        end
        object Button6: TButton
          Left = 97
          Top = 203
          Width = 75
          Height = 25
          Caption = #1059#1076#1072#1083#1080#1090#1100
          TabOrder = 3
        end
        object CheckBox10: TCheckBox
          Left = 16
          Top = 240
          Width = 185
          Height = 17
          Caption = #1055#1077#1088#1077#1079#1072#1087#1091#1089#1082' '#1079#1072#1075#1088#1091#1079#1082#1080' '#1087#1088#1080' '#1086#1096#1080#1073#1082#1077
          TabOrder = 4
        end
        object SpinEdit1: TSpinEdit
          Left = 16
          Top = 282
          Width = 185
          Height = 22
          MaxValue = 1000
          MinValue = 0
          TabOrder = 5
          Value = 0
        end
      end
    end
    object TabSheet5: TTabSheet
      Caption = 'TabSheet5'
      ImageIndex = 4
      TabVisible = False
      object GroupBox5: TGroupBox
        Left = 0
        Top = 0
        Width = 404
        Height = 357
        Align = alClient
        Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1080' '#1072#1074#1090#1086#1084#1072#1090#1080#1079#1072#1094#1080#1080': '
        TabOrder = 0
        object CheckBox12: TCheckBox
          Left = 17
          Top = 41
          Width = 248
          Height = 17
          Caption = #1042#1099#1082#1083#1102#1095#1072#1090#1100' '#1055#1050' '#1087#1086' '#1079#1072#1074#1077#1088#1096#1077#1085#1080#1080' '#1074#1089#1077' '#1079#1072#1075#1088#1091#1079#1086#1082
          TabOrder = 0
        end
        object CheckBox11: TCheckBox
          Left = 17
          Top = 18
          Width = 272
          Height = 17
          Caption = #1057#1090#1072#1088#1090#1086#1074#1072#1090#1100' '#1074#1089#1077' '#1079#1072#1075#1088#1091#1079#1082#1080' '#1087#1088#1080' '#1079#1072#1087#1091#1089#1082#1077' '#1087#1088#1086#1075#1088#1072#1084#1084#1099
          TabOrder = 1
        end
      end
    end
    object TabSheet6: TTabSheet
      Caption = 'TabSheet6'
      ImageIndex = 5
      TabVisible = False
      object GroupBox6: TGroupBox
        Left = 0
        Top = 0
        Width = 404
        Height = 357
        Align = alClient
        Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1080' '#1074#1085#1077#1096#1085#1077#1075#1086' '#1074#1080#1076#1072': '
        TabOrder = 0
        object CheckBox13: TCheckBox
          Left = 16
          Top = 24
          Width = 273
          Height = 17
          Caption = #1054#1090#1082#1088#1099#1074#1072#1090#1100' '#1086#1082#1085#1086' '#1087#1086#1089#1083#1077' '#1086#1082#1086#1085#1095#1072#1085#1080#1103' '#1074#1089#1077#1093' '#1079#1072#1075#1088#1091#1079#1086#1082
          TabOrder = 0
        end
        object CheckBox14: TCheckBox
          Left = 16
          Top = 47
          Width = 218
          Height = 17
          Caption = #1054#1090#1082#1088#1099#1074#1072#1090#1100' '#1086#1082#1085#1086' '#1077#1089#1083#1080' '#1074#1086#1079#1085#1080#1082#1083#1072' '#1086#1096#1080#1073#1082#1072
          TabOrder = 1
        end
        object CheckBox15: TCheckBox
          Left = 16
          Top = 70
          Width = 281
          Height = 17
          Caption = #1058#1088#1077#1073#1086#1074#1072#1090#1100' '#1087#1086#1076#1090#1074#1077#1088#1078#1076#1077#1085#1080#1077' '#1087#1088#1080' '#1091#1076#1072#1083#1077#1085#1080#1080' '#1079#1072#1075#1088#1091#1079#1082#1080
          TabOrder = 2
        end
      end
    end
    object TabSheet7: TTabSheet
      Caption = 'TabSheet7'
      ImageIndex = 6
      TabVisible = False
      object GroupBox7: TGroupBox
        Left = 0
        Top = 0
        Width = 404
        Height = 357
        Align = alClient
        Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1080' '#1075#1086#1088#1103#1095#1080#1093' '#1082#1083#1072#1074#1080#1096': '
        TabOrder = 0
      end
    end
    object TabSheet8: TTabSheet
      Caption = 'TabSheet8'
      ImageIndex = 7
      TabVisible = False
      object GroupBox8: TGroupBox
        Left = 0
        Top = 0
        Width = 404
        Height = 357
        Align = alClient
        Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1080' '#1080#1089#1090#1086#1088#1080#1080': '
        TabOrder = 0
        object CheckBox17: TCheckBox
          Left = 16
          Top = 24
          Width = 97
          Height = 17
          Caption = #1042#1077#1089#1090#1080' '#1080#1089#1090#1086#1088#1080#1102
          TabOrder = 0
        end
        object Button7: TButton
          Left = 16
          Top = 47
          Width = 121
          Height = 25
          Caption = #1054#1095#1080#1089#1090#1080#1090#1100' '#1080#1089#1090#1086#1088#1080#1102
          TabOrder = 1
        end
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 367
    Width = 567
    Height = 41
    Align = alBottom
    TabOrder = 1
    object Button1: TButton
      Left = 480
      Top = 6
      Width = 75
      Height = 25
      Caption = #1054#1090#1084#1077#1085#1072
      TabOrder = 0
    end
    object Button2: TButton
      Left = 399
      Top = 6
      Width = 75
      Height = 25
      Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
      TabOrder = 1
    end
    object Button3: TButton
      Left = 318
      Top = 6
      Width = 75
      Height = 25
      Caption = #1054#1082
      TabOrder = 2
    end
  end
  object TreeView1: TTreeView
    Left = 0
    Top = 0
    Width = 155
    Height = 367
    Align = alLeft
    BorderStyle = bsNone
    HideSelection = False
    Indent = 25
    ParentShowHint = False
    ReadOnly = True
    ShowHint = False
    TabOrder = 2
    OnChange = TreeView1Change
    Items.NodeData = {
      0306000000280000000000000000000000FFFFFFFFFFFFFFFF00000000000000
      000100000001051E043104490438043504320000000000000000000000FFFFFF
      FFFFFFFFFF000000000000000000000000010A18043D04420435043304400430
      04460438044F04320000000000000000000000FFFFFFFFFFFFFFFF0000000000
      00000000000000010A21043E043504340438043D0435043D04380435042E0000
      000000000000000000FFFFFFFFFFFFFFFF000000000000000000000000010817
      04300433044004430437043A043804380000000000000000000000FFFFFFFFFF
      FFFFFF000000000000000000000000010D1004320442043E043C043004420438
      0437043004460438044F04300000000000000000000000FFFFFFFFFFFFFFFF00
      0000000000000001000000010918043D0442043504400444043504390441043C
      0000000000000000000000FFFFFFFFFFFFFFFF00000000000000000000000001
      0F13043E0440044F0447043804350420003A043B04300432043804480438042C
      0000000000000000000000FFFFFFFFFFFFFFFF00000000000000000000000001
      071804410442043E04400438044F04}
  end
end
