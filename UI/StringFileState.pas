unit StringFileState;

interface

uses
  States, StrUtils;

function FileStateToString(FileState: TFileState): string;
function StringToFileState(Value: string): TFileState;

implementation

function FileStateToString(FileState: TFileState): string;
begin
  case FileState of
    fsGetInfo:
      Result := 'fsGetInfo';
    fsDownloading:
      Result := 'fsDownloading';
    fsComplete:
      Result := 'fsComplete';
    fsStopped:
      Result := 'fsStopped';
    fsError:
      Result := 'fsError';
  end;
end;

function StringToFileState(Value: string): TFileState;
begin
  case AnsiIndexStr(Value, ['fsGetInfo', 'fsDownloading', 'fsComplete',
    'fsStopped', 'fsError']) of
    0:
      Result := fsGetInfo;
    1:
      Result := fsDownloading;
    2:
      Result := fsComplete;
    3:
      Result := fsStopped;
    4:
      Result := fsError;
  else
    Result := fsError;
  end;
end;

end.
