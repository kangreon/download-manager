unit BottomBar;

interface

uses
  Graphics, Controls, Classes, Windows, SysUtils, SkinButton, ExtCtrls, Graphic,
  DateUtils, PngImage;

const
  TextMarginLeft = 10;
  TextMarginTop = 14;
  MarginLeftColumn = 30;
  MarginSecondColumn = 150;
  BarHeight = 75;
  BarMarginLeft = 145;

type
  TBottomBar = class(TCustomControl)
  private
    FTextHeight: Integer;
    FMarginLeftColumn: Integer;
    FGraphBtn: TSkinButton;
    FBytesDownloaded: Int64;
    FFileSize: Int64;
    FPartsDownloaded: Integer;
    FAllParts: Integer;
    FSpeed: Int64;
    FTime: Double;
    FPaintGraph: Boolean;
    FPaintInf: Boolean;
    FTimer: TTimer;
    FGraphic: TGraph;
    FGraphValue: Integer;
    FTimeNewValue: TDateTime;
    FSumTime: Int64;
    FOnGraphBtnClick: TNotifyEvent;
    procedure PaintBackground;
    procedure PaintBytesDownloaded(Bytes: Int64);
    procedure PaintSize(Size: Int64);
    procedure PaintParts(PartsDownloaded, AllParts: Integer);
    procedure PaintSpeed(Speed: Int64);
    procedure PaintTime(Time: Double);
    procedure PaintInfText(Text: string);
    procedure InitText;
    procedure SetGraphBtn(X: Integer);
    function FormatByteSize(const Bytes: Integer): string;
    procedure GraphBtnClick(Sender: TObject);
    procedure GraphStep(Sender: TObject);
    procedure EventGraphBtnClick(Sender: TObject);
  protected
    procedure CreateWnd; override;
    procedure Paint; override;
    procedure Resize; override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    procedure SetGraphToZero;

    property BytesDownloaded: Int64 write FBytesDownloaded;
    property FileSize: Int64 write FFileSize;
    property PartsDownloaded: Integer write FPartsDownloaded;
    property AllParts: Integer write FAllParts;
    property Speed: Int64 write FSpeed;
    property Time: Double write FTime;

    property PaintGraph: Boolean read FPaintGraph;
    property PaintInf: Boolean read FPaintInf write FPaintInf;

    property GraphValue: Integer write FGraphValue;

    property OnGraphBtnClick: TNotifyEvent read FOnGraphBtnClick
      write FOnGraphBtnClick;
  end;

implementation

{ TBottomBar }

constructor TBottomBar.Create(AOwner: TComponent);
begin
  inherited;

  FPaintInf := False;
  FPaintGraph := False;

  FTimer := TTimer.Create(nil);
  FTimer.Interval := 50;
  FTimer.OnTimer := GraphStep;
  FTimer.Enabled := False;
end;

procedure TBottomBar.CreateWnd;

begin
  inherited;

  SetGraphBtn(0);
  InitText;
end;

destructor TBottomBar.Destroy;
begin

  inherited;
end;

procedure TBottomBar.EventGraphBtnClick(Sender: TObject);
begin
  if Assigned(FOnGraphBtnClick) then
    FOnGraphBtnClick(Self);
end;

procedure TBottomBar.InitText;
begin
  Canvas.Font.Name := 'Tahoma';
  Canvas.Font.Size := 10;
  Canvas.Font.Color := RGB(42, 43, 44);
  Canvas.Brush.Style := bsClear;

  FTextHeight := Canvas.TextHeight('T');
end;

procedure TBottomBar.Paint;
begin
  inherited;

  PaintBackground;

  InitText;

  if not FPaintGraph then
  begin
    FTimer.Enabled := False;

    if FPaintInf then
    begin
      PaintBytesDownloaded(FBytesDownloaded);
      PaintSize(FFileSize);
      PaintParts(FPartsDownloaded, FAllParts);
      PaintSpeed(FSpeed);
      PaintTime(FTime);
    end
    else
      PaintInfText('��� ����������.');

    FGraphBtn.Left := ClientWidth - 30;
  end
  else
  begin
    FTimer.Enabled := True;
  end;
end;

procedure TBottomBar.PaintBackground;
begin
  Canvas.Brush.Color := RGB(240, 240, 240);
  Canvas.FillRect(Canvas.ClipRect);
end;

procedure TBottomBar.PaintBytesDownloaded(Bytes: Int64);
var
  Str: string;
begin
  Str := '���������: ' + FormatByteSize(Bytes);
  Canvas.TextOut(TextMarginLeft, TextMarginTop, Str);
end;

procedure TBottomBar.PaintInfText(Text: string);
var
  X, Y: Integer;
begin
  Canvas.Font.Name := 'Tahoma';
  Canvas.Font.Size := 16;
  Canvas.Font.Color := RGB(42, 43, 44);
  Canvas.Brush.Style := bsClear;

  X := TextMarginLeft;
  Y := (BarHeight - Canvas.TextHeight(Text)) div 2;
  Canvas.TextOut(X, Y, Text);
end;

procedure TBottomBar.PaintParts(PartsDownloaded, AllParts: Integer);
var
  Str: string;
begin
  Str := '�����: ' + IntToStr(PartsDownloaded) + '/' + IntToStr(AllParts);
  Canvas.TextOut(TextMarginLeft, TextMarginTop + FTextHeight * 2, Str);
end;

procedure TBottomBar.PaintSize(Size: Int64);
var
  Str: string;
begin
  Str := '������: ' + FormatByteSize(Size);
  Canvas.TextOut(TextMarginLeft, TextMarginTop + FTextHeight, Str);
end;

procedure TBottomBar.PaintSpeed(Speed: Int64);
var
  Str: string;
begin
  Str := '������� ��������: ' + FormatByteSize(Speed) + '/�';
  FMarginLeftColumn := TextMarginLeft + MarginSecondColumn;

  Canvas.TextOut(MarginLeftColumn + FMarginLeftColumn, TextMarginTop, Str);

end;

procedure TBottomBar.PaintTime(Time: Double);
var
  Str: string;
begin
  Str := '���������� �����: ' + TimeToStr(Time);

  Canvas.TextOut(MarginLeftColumn + FMarginLeftColumn,
    TextMarginTop + FTextHeight, Str);
end;

procedure TBottomBar.Resize;
begin
  inherited;

end;

procedure TBottomBar.SetGraphBtn(X: Integer);
var
  PngImage: TPngImage;
begin
  FGraphBtn := TSkinButton.Create(Self);
  FGraphBtn.Parent := Self;
  FGraphBtn.Top := 1;
  FGraphBtn.Left := ClientWidth - 30;
  FGraphBtn.Visible := True;

  PngImage := TPngImage.Create;
  PngImage.LoadFromResourceName(0, 'garrow');
  FGraphBtn.Picture.Graphic := PngImage;

  FGraphBtn.OnClick := GraphBtnClick;
end;

procedure TBottomBar.SetGraphToZero;
begin
  if Assigned(FGraphic) then
    FGraphic.SetGraphToZero;
end;

function TBottomBar.FormatByteSize(const Bytes: Longint): string;
const
  B = 1; // byte
  KB = 1024 * B; // kilobyte
  MB = 1024 * KB; // megabyte
  GB = 1024 * MB; // gigabyte
begin
  if Bytes = 0 then
    Result := '0 �'
  else if Bytes > GB then
    Result := FormatFloat('#.## ���', Bytes / GB)
  else if Bytes > MB then
    Result := FormatFloat('#.## ���', Bytes / MB)
  else if Bytes > KB then
    Result := FormatFloat('#.## ���', Bytes / KB)
  else
    Result := FormatFloat('#.## �', Bytes);
end;

procedure TBottomBar.GraphBtnClick(Sender: TObject);
var
  PngImage: TPngImage;
begin
  if not FPaintGraph then
  begin
    PngImage := TPngImage.Create;
    PngImage.LoadFromResourceName(0, 'garrow2');
    FGraphBtn.Picture.Graphic := PngImage;
    FGraphBtn.Left := 0;
    PngImage.Free;

    FPaintGraph := True;

    FGraphic := TGraph.Create(Self);
    FGraphic.Parent := Self;

    FGraphic.Align := alClient;
    FGraphic.AlignWithMargins := True;
    FGraphic.Margins.Left := 30;
    FGraphic.Margins.Top := 0;
    FGraphic.Margins.Bottom := 0;
    FGraphic.Margins.Right := 0;

    FTimer.Enabled := True;
  end
  else
  begin
    PngImage := TPngImage.Create;
    PngImage.LoadFromResourceName(0, 'garrow');
    FGraphBtn.Picture.Graphic := PngImage;
    FGraphBtn.Left := ClientWidth - 30;
    PngImage.Free;

    FPaintGraph := False;
    FTimer.Enabled := False;
    FGraphic.Free;
  end;

  EventGraphBtnClick(Self);
end;

procedure TBottomBar.GraphStep(Sender: TObject);
var
  Time: Int64;
begin
  Time := MilliSecondsBetween(Now, FTimeNewValue);

  FTimeNewValue := Now;
  FSumTime := FSumTime + Time;
  if FSumTime > 500 then
  begin
    FSumTime := FSumTime - 500;
    FGraphic.NewValue(FGraphValue);
  end;
end;

end.
