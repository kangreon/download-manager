unit Graphic;

interface

uses
  Windows, Graphics, Controls, Classes, SysUtils, GdiPlus;

type
  TGraph = class(TGraphicControl)
  private
    FValues: array of TGPPointF;
    FBackGroundColor: TColor;
    FBackGroundLinesColor: TColor;
    FGraphicColor: TColor;
    FRate: Single;
    FOldRate: Single;
    FImage: IGPGraphics;
    procedure PaintBackground;
    procedure PaintGraphic;
    procedure ReCalcGraphic;
  protected
    procedure Paint; override;
    procedure Resize; override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    procedure NewValue(const Value: Integer);
    procedure SetGraphToZero;

    property BackGroundColor: TColor read FBackGroundColor
      write FBackGroundColor;
    property BackGroundLinesColor: TColor read FBackGroundLinesColor
      write FBackGroundLinesColor;
    property GraphicColor: TColor read FGraphicColor write FGraphicColor;
  end;

implementation

{ TGraph }

constructor TGraph.Create(AOwner: TComponent);
begin
  inherited;

  FBackGroundColor := clWhite;
  FBackGroundLinesColor := RGB(240, 240, 240);
  FGraphicColor := RGB(123, 59, 73);

  SetGraphToZero;
end;

destructor TGraph.Destroy;
begin

  inherited;
end;

procedure TGraph.NewValue(const Value: Integer);
begin
  SetLength(FValues, Length(FValues) + 1);

  FValues[Length(FValues) - 1].X := ClientWidth;
  FValues[Length(FValues) - 1].Y := Value;

  if FValues[High(FValues)].Y = 0 then
    FValues[High(FValues)].Y := 1;

  if Length(FValues) = 1 then
    FOldRate := ClientHeight / FValues[High(FValues)].Y;

  ReCalcGraphic;

  Invalidate;
end;

procedure TGraph.Paint;
begin
  inherited;

  FImage := TGPGraphics.Create(Canvas.Handle);
  FImage.SmoothingMode := SmoothingModeAntiAlias;

  PaintBackground;
  PaintGraphic;
end;

procedure TGraph.PaintBackground;
var
  I: Integer;
begin
  Canvas.Brush.Color := FBackGroundColor;
  Canvas.FillRect(Canvas.ClipRect);

  Canvas.Pen.Color := FBackGroundLinesColor;
  Canvas.Pen.Width := 1;

  // Горизонтальные линии
  I := 0;
  while I <= ClientHeight do
  begin
    Canvas.MoveTo(0, I);
    Canvas.LineTo(ClientWidth, I);

    Inc(I, 15);
  end;

  // Вертикальные линии
  I := 0;
  while I <= ClientWidth do
  begin
    Canvas.MoveTo(I, 0);
    Canvas.LineTo(I, ClientHeight);

    Inc(I, 15);
  end;
end;

procedure TGraph.PaintGraphic;
var
  Pen: IGPPen;
  I: Integer;
begin
  if Length(FValues) < 2 then
    Exit;

  Pen := TGPPen.Create(TGPColor.Create(GetRValue(FGraphicColor),
    GetGValue(FGraphicColor), GetBValue(FGraphicColor)), 1);
  FImage.DrawLines(Pen, FValues);

  for I := 0 to Length(FValues) do
    FValues[I].X := FValues[I].X - 2;
end;

procedure TGraph.ReCalcGraphic;
var
  I: Integer;
  Value: Single;
begin
  Value := FValues[High(FValues)].Y;

  FRate := ClientHeight / Value;

  if FRate < FOldRate then
  begin
    for I := Low(FValues) to High(FValues) do
      FValues[I].Y := ClientHeight - FValues[I].Y / FOldRate * FRate;

    FOldRate := FRate;
  end
  else
    FValues[High(FValues)].Y := ClientHeight - FValues[High(FValues)].Y
      * FOldRate;

  for I := Low(FValues) to High(FValues) do
    if FValues[I].Y > ClientHeight then
      FValues[I].Y := ClientHeight
    else if FValues[I].Y < 0 then
      FValues[I].Y := 0;
end;

procedure TGraph.Resize;
begin
  inherited;

end;

procedure TGraph.SetGraphToZero;
begin
  FRate := 0;
  FOldRate := 0;

  SetLength(FValues, 0);
end;

end.
