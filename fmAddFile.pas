unit fmAddFile;

interface

uses
  Forms, StdCtrls, Controls, Classes, Vcl.ExtCtrls, FileCtrl;

type
  TAddFileForm = class(TForm)
    Edit1: TEdit;
    Edit2: TEdit;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Label1: TLabel;
    Label2: TLabel;
    CheckBox1: TCheckBox;
    Memo1: TMemo;
    CheckBox2: TCheckBox;
    Label3: TLabel;
    GroupBox1: TGroupBox;
    Panel1: TPanel;
    Label4: TLabel;
    ComboBox1: TComboBox;
    procedure Button3Click(Sender: TObject);
    procedure Edit1Change(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CheckBox2Click(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    FUrl: string;
    FPath: string;
    FOnGetData: TNotifyEvent;
    procedure EventGetData;
  public
    property Url: string read FUrl;
    property Path: string read FPath;
    property OnGetData: TNotifyEvent read FOnGetData write FOnGetData;
  end;

var
  AddFileForm: TAddFileForm;

implementation

{$R *.dfm}

procedure TAddFileForm.Button1Click(Sender: TObject);
var
  sDir: String;
begin
  if SelectDirectory('�������� �������', '', sDir) then
    Edit2.Text := sDir;
end;

procedure TAddFileForm.Button2Click(Sender: TObject);
begin
  FUrl := Edit1.Text;
  FPath := Edit2.Text;

  EventGetData;

  Close;
end;

procedure TAddFileForm.Button3Click(Sender: TObject);
begin
  Close;
end;

procedure TAddFileForm.CheckBox1Click(Sender: TObject);
begin
  if CheckBox1.Checked then
  begin
    Label3.Enabled := True;
    Memo1.Enabled := True;
    Label1.Enabled := False;
    Edit1.Enabled := False;
  end
  else
  begin
    Label3.Enabled := False;
    Memo1.Enabled := False;
    Label1.Enabled := True;
    Edit1.Enabled := True;
  end;
end;

procedure TAddFileForm.CheckBox2Click(Sender: TObject);
begin
  if CheckBox2.Checked then
  begin
    Label2.Enabled := False;
    Edit2.Enabled := False;
    Button1.Enabled := False;
  end
  else
  begin
    Label2.Enabled := True;
    Edit2.Enabled := True;
    Button1.Enabled := True;
  end;
end;

procedure TAddFileForm.Edit1Change(Sender: TObject);
begin
  if (Edit1.Text <> '') and (Edit2.Text <> '') then
    Button2.Enabled := True
  else
    Button2.Enabled := False;
end;

procedure TAddFileForm.EventGetData;
begin
  if Assigned(FOnGetData) then
    FOnGetData(Self);
end;

procedure TAddFileForm.FormCreate(Sender: TObject);
begin
  Position := poScreenCenter;
end;

end.
