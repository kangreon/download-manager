unit fmMain;

interface

uses
  Vcl.Forms, InitUI, System.Classes, Vcl.Controls, VirtualTrees;

type
  TMainForm = class(TForm)
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    FFlag: Boolean;
    FUInterface: TUInterface;
  public
    { Public declarations }
  end;

var
  MainForm: TMainForm;

implementation

{$R *.dfm}

procedure TMainForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  FUInterface.Free;
end;

procedure TMainForm.FormCreate(Sender: TObject);
begin
  ClientWidth := 770;
  ClientHeight := 390;

  Constraints.MinWidth := Width;
  Constraints.MinHeight := Height;

  Position := poScreenCenter;

  FUInterface := TUInterface.Create;
  FUInterface.InitComponents(Self);

  FFlag := True;
end;

procedure TMainForm.FormResize(Sender: TObject);
begin
  if FFlag then
    FUInterface.ResizeComponents(Self);
end;

end.
