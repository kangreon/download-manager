﻿program TestDownload;

{$R *.dres}

uses
  Vcl.Forms,
  FileWriter in 'DMCore\FileWriter.pas',
  FileDownload in 'DMCore\FileDownload.pas',
  InetUtils in 'DMCore\Utils\InetUtils.pas',
  PartDownloadThread in 'DMCore\PartDownloadThread.pas',
  BufferUtils in 'DMCore\Utils\BufferUtils.pas',
  PartManager in 'DMCore\PartManager.pas',
  StreamUtils in 'DMCore\Utils\StreamUtils.pas',
  PartInfo in 'DMCore\PartInfo.pas',
  States in 'DMCore\States.pas',
  PartDownload in 'DMCore\PartDownload.pas',
  LeftBar in 'UI\LeftBar\LeftBar.pas',
  TopBar in 'UI\TopBar\TopBar.pas',
  BottomBar in 'UI\BottomBar\BottomBar.pas',
  fmMain in 'fmMain.pas' {MainForm},
  fmAddFile in 'UI\fmAddFile.pas' {AddFileForm},
  fmSettings in 'UI\fmSettings.pas' {SettingsForm},
  DownloadBar in 'UI\DownloadBar\DownloadBar.pas',
  Downloads in 'UI\Downloads.pas',
  InitUI in 'UI\InitUI.pas',
  Graphic in 'UI\BottomBar\Graphic.pas',
  GdiPlus in 'gdi\GdiPlus.pas',
  FileAdd in 'UI\FileAdd.pas',
  Comparer in 'UI\Comparer.pas',
  FormatSettings in 'UI\FormatSettings.pas',
  DBUtils in 'DatabaseUtils\DBUtils.pas',
  StringFileState in 'UI\StringFileState.pas',
  SkinButton in 'UI\SkinButton.pas',
  HistoryList in 'DatabaseUtils\HistoryList.pas',
  DBTypes in 'DatabaseUtils\DBTypes.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TMainForm, MainForm);
  Application.CreateForm(TSettingsForm, SettingsForm);
  Application.Run;

end.
