﻿unit test;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FileDownload, Vcl.StdCtrls,
  Vcl.ComCtrls, States, fmMain, Vcl.ExtCtrls, VirtualTrees;

type
	//Форма тест
  TForm2 = class(TForm)
    Edit1: TEdit;
    btnStart: TButton;
    Label1: TLabel;
    ProgressBar1: TProgressBar;
    btnStop: TButton;
    lblState: TLabel;
    Button1: TButton;
    Label2: TLabel;
    procedure btnStartClick(Sender: TObject);
    procedure btnStopClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure VirtualStringTree1PaintText(Sender: TBaseVirtualTree;
      const TargetCanvas: TCanvas; Node: PVirtualNode; Column: TColumnIndex;
      TextType: TVSTTextType);
  private
    FFileDownload: TFileDownload;
    FIsDownloadFile: Boolean;
    procedure BufferDowload(Sender: TObject);
    function FormatByteSize(const bytes: Integer): string;
    procedure DownloadEnd(Sender: TObject; FileState: TFileState;
      ThreadID: Integer);
    procedure StartDownload(Link, Path: string);
    procedure ComponentState(IsDownload: Boolean);
    procedure StopDownload;
  public
    { Public declarations }
  end;

var
  Form2: TForm2;

implementation

{$R *.dfm}

{ *  Начало загрузки файла
  * }
procedure TForm2.StartDownload(Link, Path: string);
begin
  if FIsDownloadFile then
    Exit;

  FIsDownloadFile := True;
  ComponentState(True);

  FFileDownload := TFileDownload.Create(Path, Link, 1, 4, '');
  FFileDownload.OnBufferDownload := BufferDowload;
  FFileDownload.OnDownloadEnd := DownloadEnd;
  FFileDownload.Start;
end;

{ *  Останавливает скачивание
  * }
procedure TForm2.StopDownload;
begin
  if not FIsDownloadFile then
    Exit;

  ComponentState(False);

  FFileDownload.StopDownload;
end;

procedure TForm2.VirtualStringTree1PaintText(Sender: TBaseVirtualTree;
  const TargetCanvas: TCanvas; Node: PVirtualNode; Column: TColumnIndex;
  TextType: TVSTTextType);
begin

end;

{ *  Устанавливает активность компонентов в зависимости от состояния загрузки
  * }
procedure TForm2.ComponentState(IsDownload: Boolean);
begin
  FIsDownloadFile := IsDownload;

  btnStart.Enabled := not IsDownload;
  btnStop.Enabled := IsDownload;
  Edit1.Enabled := not IsDownload;
end;

procedure TForm2.BufferDowload(Sender: TObject);
begin
  Label1.Caption := 'Скорость скачивания: ' +
    FormatByteSize(FFileDownload.DownloadSpeed) + '/с';

  ProgressBar1.Position := 100 * FFileDownload.BytesDownloaded div
    FFileDownload.FileSize;

  Label2.Caption := 'Загружено: ' + FormatByteSize
    (FFileDownload.BytesDownloaded);
end;

procedure TForm2.Button1Click(Sender: TObject);
begin
  MainForm.Show;
end;

procedure TForm2.DownloadEnd(Sender: TObject; FileState: TFileState;
  ThreadID: Integer);
begin
  StopDownload;
end;

procedure TForm2.btnStartClick(Sender: TObject);
begin
  StartDownload(Edit1.Text, ExtractFilePath(ParamStr(0)));
end;

procedure TForm2.btnStopClick(Sender: TObject);
begin
  StopDownload;
end;

function TForm2.FormatByteSize(const bytes: Longint): string;
const
  B = 1; // byte
  KB = 1024 * B; // kilobyte
  MB = 1024 * KB; // megabyte
  GB = 1024 * MB; // gigabyte
begin
  if bytes = 0 then
    Result := '0 б'
  else if bytes > GB then
    Result := FormatFloat('#.## ГиБ', bytes / GB)
  else if bytes > MB then
    Result := FormatFloat('#.## МиБ', bytes / MB)
  else if bytes > KB then
    Result := FormatFloat('#.## КиБ', bytes / KB)
  else
    Result := FormatFloat('#.## б', bytes);
end;

procedure TForm2.FormCreate(Sender: TObject);
begin
  ComponentState(False);
end;

end.
